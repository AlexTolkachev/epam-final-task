package by.epam.tests;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.orders.Order;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.junit.Assert;
import org.junit.Test;
/**
 * Created by User on 26.06.2016.
 */


public class JUnitTest {
    @Test
    public void testUsersUsernameSetter(){
        User user = new User();
        String username = "Petya";
        user.setUsername(username);

        boolean excpectedRes = true;
        Assert.assertEquals(excpectedRes, user.getUsername().equals(username));
    }

    @Test
    public void testForExcpectedError(){
        User user = new User();
        String email = "blabla.com";
        try {
            user.seteMail(email);
        } catch (EmailException e) {
            e.printStackTrace();
        }
        boolean excpectedResult = true;
        Assert.assertEquals(excpectedResult, user.geteMail() == null);
    }

    @Test
    public void testPackCountOrder(){
        Order order = new Order();
        try {
            order.setPackageCount(100000000);
        } catch (OutOfRangeException e) {
            e.printStackTrace();
        }
        int excepectedResult = 0;
        Assert.assertEquals(excepectedResult, order.getPackageCount());
    }
}
