package by.epam.controller.internationalization;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by User on 22.06.2016.
 */

@WebServlet("/changeLanguage")
public class LanguageController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }
    private static final Logger log = Logger.getLogger(String.valueOf(LanguageController.class));

    /**
     * processes submit click on changing language selector
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String language = req.getParameter("language");
        String previousURL = req.getHeader("referer");
        log.info("Previous url:" + previousURL);
        HttpSession session = req.getSession(false);
        session.setAttribute("language", language);
        resp.sendRedirect(previousURL);
    }
}
