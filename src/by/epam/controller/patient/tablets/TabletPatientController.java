package by.epam.controller.patient.tablets;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.orders.dao.JDBCOrderDAO;
import by.epam.model.tablets.Tablet;
import by.epam.model.tablets.dao.JDBCTabletDAO;
import by.epam.model.tablets.types.TabletType;
import by.epam.model.users.users.User;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 14.05.2016.
 */

@WebServlet("/patient/tabletList")
public class TabletPatientController extends HttpServlet{
    static {
        new DOMConfigurator().doConfigure("D:\\EPAM Training\\Final Task\\EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(TabletPatientController.class));
    private static final String LIST_TABLETS_PAGE = "view/product/listTablets.jsp";
    private JDBCTabletDAO tabletDAO;
    private JDBCOrderDAO orderDAO;

    /**
     * init's DAO's
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        log.info("TABLET LIST Controller INIT");
        try{
            orderDAO = new JDBCOrderDAO();
            tabletDAO = new JDBCTabletDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * processes url for interacting with patient's tablet list menu
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("TABLET LIST Controller before select");
        HttpSession session = req.getSession();
        User user = (User)session.getAttribute("user");
        int page = 1;
        int recordsPerPage = 8;
        if(req.getParameter("page") != null)
            page = Integer.parseInt(req.getParameter("page"));

        int typeId = 0;
        String strTypeId = null;
        String searchPattern = req.getParameter("tablet_name");
        log.info(searchPattern);

        if(searchPattern == null)
            strTypeId = req.getParameter("type");
        if(strTypeId != null)
            typeId = Integer.parseInt(req.getParameter("type"));

        Set<Tablet> tablets = new HashSet<>();
        try {
            if(searchPattern != null) {
                for (Tablet t : tabletDAO.select()){
                    log.info("Tablet name: " + t.getName());
                    if(t.getName().equalsIgnoreCase(searchPattern)){
                        tablets.add(t);
                        log.info("Found Tablet!");
                    }
                }
            } else if(typeId == 0)
                tablets = tabletDAO.viewAllTablets((page-1)*recordsPerPage,
                    recordsPerPage);
            else
                tablets = tabletDAO.viewAllTabletsByType((page-1)*recordsPerPage,
                        recordsPerPage, typeId);

            int noOfRecords = tabletDAO.getNoOfRecords();
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

            req.setAttribute("username", user.getUsername());
            req.setAttribute("patient", user);
            req.setAttribute("noOfPages", noOfPages);
            req.setAttribute("currentPage", page);


            req.setAttribute("tablets", tablets);
            req.setAttribute("orders", orderDAO.select());
            req.setAttribute("types", TabletType.values());
            req.getRequestDispatcher(LIST_TABLETS_PAGE).forward(req, resp);
        } catch (OutOfRangeException e) {
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }


}
