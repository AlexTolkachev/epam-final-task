package by.epam.controller.patient.profile;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.dao.JDBCUserDAO;
import by.epam.model.users.patients.JDBCPatientInfoDAO;
import by.epam.model.users.patients.Patient;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/**
 * Created by User on 20.05.2016.
 */

@WebServlet("/patient/account")
public class AccountController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }
    /**
     * value for storing login page directory
     */
    private static final String LOGIN_PAGE = "view/account/login.jsp";
    /**
     * value for storing patient home menu url
     */
    private static final String PATIENT_MENU = "/patient/tabletList";
    /**
     * value for storing doctor order calls url
     */
    private static final String DOCTOR_MENU = "/doctor/order_calls";
    /**
     * value for storing pharmasist menu url
     */
    private static final String PHARMASIST_MENU = "/pharmasist/tablets";
    /**
     * represents doctors role id constant for patient
     */
    private static final int ROLE_ID = 1;

    private static final Logger log = Logger.getLogger(String.valueOf(AccountController.class));
    private JDBCUserDAO userDAO;
    private JDBCPatientInfoDAO patientDAO;

    /**
     * init's DAO's
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try {
            userDAO = new JDBCUserDAO();
            patientDAO = new JDBCPatientInfoDAO();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Processes url's for managing patient's account. (e.g. log in and log out with {@value #LOGIN_PAGE})
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("In Get Controller");
        HttpSession session = req.getSession();
        session.removeAttribute("user");
        req.getRequestDispatcher(LOGIN_PAGE).forward(req, resp);
    }

    /**
     * Processes urls for user authentification and registration
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String action = req.getParameter("action");
        List<User> users = null;

        boolean isFound = false;

        if(action == null || action.isEmpty()) {
            try {
                users = userDAO.select();
                for (User u : users) {
                    if (username.equalsIgnoreCase(u.getUsername()) && password.equalsIgnoreCase(u.getPassword())) {
                        log.info("found user!");
                        isFound = true;
                        HttpSession session = req.getSession();
                        session.setAttribute("user", u);
                        if (u.getRoleId() == 1)
                            resp.sendRedirect(PATIENT_MENU);
                        else if (u.getRoleId() == 2)
                            resp.sendRedirect(DOCTOR_MENU);
                        else if (u.getRoleId() == 3)
                            resp.sendRedirect(PHARMASIST_MENU);
                        }
                    }
                    if (!isFound) {
                        log.info("Found NOONE! for username:" + username + " and password:" + password);
                        req.setAttribute("message", "Account's Invalid");
                        req.getRequestDispatcher(LOGIN_PAGE).forward(req, resp);
                    }
            } catch (EmailException | OutOfRangeException e) {
                e.printStackTrace();
                resp.getWriter().write(e.toString());
            }
        }else if(action.equalsIgnoreCase("register")){

            try {
                Patient patient = new Patient(req.getParameter("username_register"),
                            req.getParameter("password_register"), req.getParameter("name"),
                                req.getParameter("surname"), req.getParameter("adress"), Integer.parseInt((req.getParameter("age"))),
                                    req.getParameter("telephone"), req.getParameter("e-mail"), Double.parseDouble(req.getParameter("virtual_cash")),
                                        Double.parseDouble(req.getParameter("credit_card_cash")), Integer.parseInt(req.getParameter("phys_group")), ROLE_ID);

                boolean multipleUsernames = false;
                for (User u : userDAO.select()){
                    if(patient.getUsername().equalsIgnoreCase(u.getUsername())){
                        req.setAttribute("message", "User with this username already exists!");
                        req.getRequestDispatcher(LOGIN_PAGE).forward(req, resp);
                        multipleUsernames = true;
                    }
                }
                if(!multipleUsernames) {
                    Lock lock = new ReentrantLock();
                    lock.lock();
                    int lastId = patientDAO.insert(patient);
                    lock.unlock();
                    patient.setUserId(lastId);
                    patient.setId(lastId);
                    HttpSession session = req.getSession();
                    session.setAttribute("user", patient);
                    resp.sendRedirect(PATIENT_MENU);
                }
            }catch (EmailException | OutOfRangeException e){
                e.printStackTrace();
                log.error(e);
                resp.getWriter().write(e.toString());
            }
        }

    }
}
