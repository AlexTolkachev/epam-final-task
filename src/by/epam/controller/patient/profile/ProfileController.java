package by.epam.controller.patient.profile;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.User;
import by.epam.model.users.dao.JDBCUserDAO;
import by.epam.model.users.patients.JDBCPatientInfoDAO;
import by.epam.model.users.patients.Patient;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 22.05.2016.
 */
@WebServlet("/patient/profile")
public class ProfileController extends HttpServlet{
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(ProfileController.class));
    /**
     * value for doctor profile page directory
     */
    private static final String PATIENT_MENU = "/patient/tabletList";
    /**
     * stores directory of patient profile page
     */
    private static final String PATIENT_PROFILE_PAGE = "view/account/patient_profile.jsp";
    /**
     * role id for Patient
     */
    private static final int ROLE_ID = 1;
    private JDBCPatientInfoDAO patientDAO;
    private JDBCUserDAO userDAO;

    /**
     * init's DAO objects
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            patientDAO = new JDBCPatientInfoDAO();
            userDAO = new JDBCUserDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }

    }
    /**
     * processes url for changing patient profile info
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("profileController get Method");
        User user = (User) req.getSession().getAttribute("user");
        Patient pat = null;
        try {
            pat = patientDAO.getPatientByUserId(user.getId());
            req.setAttribute("patient", pat);
            req.getRequestDispatcher(PATIENT_PROFILE_PAGE).forward(req, resp);
        } catch (OutOfRangeException | EmailException e) {
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
    /**
     * updates patient info after clicking submit
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("In ProfileController post method");

        try {
            Patient patient = new Patient(req.getParameter("username"),
                    req.getParameter("password"), req.getParameter("name"),
                    req.getParameter("surname"), req.getParameter("adress"), Integer.parseInt(req.getParameter("age")),
                    req.getParameter("telephone"), req.getParameter("e-mail"), Double.parseDouble(req.getParameter("virtual_cash")),
                    Double.parseDouble(req.getParameter("credit_card_cash")), Integer.parseInt(req.getParameter("phys_group")), ROLE_ID);
            patient.setUserId(Integer.parseInt(req.getParameter("patient_id")));

            String action = req.getParameter("action");

            Lock lock = new ReentrantLock();
            lock.lock();

            String password = null;
            if (action != null && action.equalsIgnoreCase("register")) {
                password = req.getParameter("password");
                patient.setPassword(password);
                userDAO.insert(patient);
                patientDAO.insert(patient);
                req.getSession().setAttribute("user", patient);
            } else {
                patientDAO.update(patient);
            }

            lock.unlock();

            resp.sendRedirect(PATIENT_MENU);
        } catch (OutOfRangeException | EmailException e) {
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
}
