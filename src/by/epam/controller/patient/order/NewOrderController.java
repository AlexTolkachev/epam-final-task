package by.epam.controller.patient.order;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.orders.Order;
import by.epam.model.orders.orders.dao.JDBCOrderDAO;
import by.epam.model.tablets.Tablet;
import by.epam.model.tablets.dao.JDBCTabletDAO;
import by.epam.model.users.users.User;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 05.06.2016.
 */
@WebServlet("/patient/neworder")
public class NewOrderController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(NewOrderController.class));
    /**
     * holds curent user object
     */
    private User currentUser;
    private JDBCTabletDAO tabletDAO;
    private JDBCOrderDAO orderDAO;

    /**
     * init's DAO's
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            tabletDAO = new JDBCTabletDAO();
            orderDAO = new JDBCOrderDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * processes url's for adding new tablet to a bucket
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int tabletId = Integer.parseInt(req.getParameter("tabletId"));

        currentUser = (User)req.getSession().getAttribute("user");

        try {
            Lock lock = new ReentrantLock();
            lock.lock();

            int lastOrderId = insertTablet(tabletId);

            lock.unlock();

            String stringId = String.valueOf(lastOrderId);

            PrintWriter out = resp.getWriter();
            out.print(stringId);
        } catch (OutOfRangeException e) {
            e.printStackTrace();
            log.error(e);
            PrintWriter out = resp.getWriter();
            out.print("Error While Adding Tablet to bucket!" + e.toString());
        }
    }

    /**
     * inserts add's tablet to a bucket(e.g add's row in order table for current user)
     * @param tabletId
     * @return
     * @throws OutOfRangeException
     */
    private int insertTablet(int tabletId) throws OutOfRangeException {
        Order order = new Order(0, 1, currentUser.getId(), false, tabletId);
        for (Tablet t : tabletDAO.select()){
            if(t.getTabletId() == tabletId){
                order.setIsAllowed(!t.isNeedRecepie());
                orderDAO.insert(order);
                break;
            }
        }
        return orderDAO.getLastInsertedId();
    }
}
