package by.epam.controller.patient.order;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.ordercalls.dao.JDBCOrderCallDAO;
import by.epam.model.orders.orders.Order;
import by.epam.model.orders.orders.dao.JDBCOrderDAO;
import by.epam.model.tablets.Tablet;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 31.05.2016.
 */
@WebServlet("/patient/bucket")
public class BucketController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    /**
     * order DAO object
     */
    private JDBCOrderDAO jdbcOrderDAO;
    /**
     * order call DAO object
     */
    private JDBCOrderCallDAO orderCallDAO;
    /**
     * holds current user info
     */
    private User currentUser;
    /**
     * stores directory for bucket page
     */
    private static final String BUCKET_PAGE = "view/order/bucket.jsp";

    /**
     * init's DAO connections
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            jdbcOrderDAO = new JDBCOrderDAO();
            orderCallDAO = new JDBCOrderCallDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    private static final Logger log = Logger.getLogger(String.valueOf(JDBCOrderDAO.class));

    /**
     * processes url's for managing and showing patients bucket
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        currentUser = (User)req.getSession().getAttribute("user");
        String action = req.getParameter("action");
        boolean accessToOrder = true;

        Lock lock = new ReentrantLock();
        lock.lock();

        if(action != null && action.equals("delete")){
            log.info("Deleting order");
            int orderId = Integer.parseInt(req.getParameter("orderId"));
            orderCallDAO.deleteByOrderId(orderId);
            jdbcOrderDAO.delete(orderId);
        }

        try {
            checkOrderForCompliance(currentUser.getId());

            lock.unlock();

            accessToOrder = blockPurchase();

            log.info("Patients Name: " + currentUser.getName());

            req.getSession().setAttribute("orders", jdbcOrderDAO.selectForPatient(currentUser.getId()));

            req.setAttribute("order_calls", orderCallDAO.select());
            req.setAttribute("access", accessToOrder);
            req.setAttribute("username", currentUser.getUsername());
            req.setAttribute("patient", currentUser);
            req.getRequestDispatcher(BUCKET_PAGE).forward(req, resp);
        } catch (EmailException | OutOfRangeException e) {
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * checks purchase for not allowed tablets(e.g. that need confirmed recepie)
     * @return
     * @throws OutOfRangeException
     */
    private boolean blockPurchase() throws OutOfRangeException {
        for (Order o : jdbcOrderDAO.selectForPatient(currentUser.getId())){
            if(!o.getIsAllowed())
                return false;
        }
        return true;
    }

    /**
     * checks tablets in order for compliance (e.g. if tablet has recepie but not allowed it chages it's allowed status and
     * updates order table in database)
     * @param userId
     * @throws OutOfRangeException
     */
    private void checkOrderForCompliance(int userId) throws OutOfRangeException {
        List<Order> orderList = jdbcOrderDAO.selectForPatient(userId);

        for (Order o : orderList){
            Tablet t = o.getTablet();
            if(!t.isNeedRecepie() && !o.getIsAllowed()) {
                o.setIsAllowed(true);
                jdbcOrderDAO.update(o);
            }/*else if(t.isNeedRecepie() && o.getIsAllowed()){
                o.setIsAllowed(false);
                jdbcOrderDAO.update(o);
            }*/
        }
    }
}
