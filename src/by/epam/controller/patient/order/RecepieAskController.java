package by.epam.controller.patient.order;

import by.epam.model.orders.ordercalls.OrderCall;
import by.epam.model.orders.ordercalls.dao.JDBCOrderCallDAO;
import by.epam.model.tablets.Tablet;
import by.epam.model.tablets.dao.JDBCTabletDAO;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.doctors.Doctor;
import by.epam.model.users.doctors.JDBCDoctorDAO;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 10.06.2016.
 */
@WebServlet("/patient/askrecepie")
public class RecepieAskController extends HttpServlet{
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    /**
     * stores url for patient bucket menu
     */
    private static final String BUCKET_MENU = "/patient/bucket";
    /**
     * stores directory for showing doctors and asking doctor for recepie
     */
    private static final String ASK_RECEPIE_PAGE = "view/order/ask_recepie.jsp";
    private static final Logger log = Logger.getLogger(String.valueOf(RecepieAskController.class));

    private JDBCOrderCallDAO orderCallDAO;
    private JDBCDoctorDAO doctorDAO;
    private JDBCTabletDAO tabletDAO;

    /**
     * init's DAO's
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            orderCallDAO = new JDBCOrderCallDAO();
            doctorDAO = new JDBCDoctorDAO();
            tabletDAO = new JDBCTabletDAO();
        }catch (SQLException e){
            e.printStackTrace();
        }catch (NamingException e){
            e.printStackTrace();
        }
    }

    /**
     * Processes url's for asking doctors for a recepie
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User patient = (User)req.getSession().getAttribute("user");
        int tabletId = Integer.parseInt(req.getParameter("tablet_id"));
        int orderId = Integer.parseInt(req.getParameter("order_id"));
        Tablet tablet = null;
        try {
            tablet = tabletDAO.getTabletById(tabletId);

            List<Doctor> doctorList = null;
            if(tablet != null)
                doctorList = doctorDAO.selectDoctorsByQualification(tabletDAO.getQualificationByTabletType(tablet.getTypeId()));

            req.setAttribute("patient", patient);
            req.setAttribute("doctors", doctorList);
            req.setAttribute("tablet", tablet);
            req.setAttribute("order_id", orderId);
            req.getRequestDispatcher(ASK_RECEPIE_PAGE).forward(req, resp);
        } catch (EmailException | OutOfRangeException e) {
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * insert's order call for chosen doctor and redirects to a bucket
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderId = req.getParameter("order_id");
        String doctorId = req.getParameter("doctor_id");
        Lock lock = new ReentrantLock();
        lock.lock();

        log.info("Get doctor ID: " + doctorId + " Order ID: " + orderId);
        orderCallDAO.insert(new OrderCall(Integer.parseInt(orderId), null, Integer.parseInt(doctorId)));    //add arder call to a doctor!
        log.info("Added order call!");

        lock.unlock();

        resp.sendRedirect(BUCKET_MENU);
    }
}
