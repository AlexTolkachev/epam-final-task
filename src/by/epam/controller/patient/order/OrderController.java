package by.epam.controller.patient.order;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.orders.Order;
import by.epam.model.orders.orders.dao.JDBCOrderDAO;
import by.epam.model.tablets.Tablet;
import by.epam.model.tablets.dao.JDBCTabletDAO;
import by.epam.model.users.patients.JDBCPatientInfoDAO;
import by.epam.model.users.patients.Patient;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 17.05.2016.
 */
@WebServlet("/patient/order")
public class OrderController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }
    private static final Logger log = Logger.getLogger(String.valueOf(OrderController.class));

    /**
     * stores action value for updating packages count in bucket
     */
    private static final String UPDATE_COUNTER = "update_counter";
    /**
     * stores action value for confirming order
     */
    private static final String ORDER_CONFIRM = "orderConfirm";
    /**
     * stores action value for managing order payment
     */
    private static final String REGISTERED_ACTION = "registeredTab";
    /**
     * stores directory for tablet info page
     */
    private static final String TABLET_INFO = "view/product/tabletInfo.jsp";
    /**
     * stores url to bucket controller
     */
    private static final String BUCKET_URL = "/patient/bucket";
    /**
     * stores directory for succeded order page
     */
    private static final String ORDER_SUCCES = "view/order/orderSuccess.jsp";
    /**
     * stores directory for order payment stage
     */
    private static final String ORDER_CONFIRM_REGISTERED = "view/order/registered.jsp";
    /**
     * stores value for payment_type constant
     */
    private static final String PAYMENT_TYPE = "payment_type";
    /**
     * stores value for user constant
     */
    private static final String USER = "user";

    private JDBCOrderDAO orderDAO;
    private JDBCTabletDAO tabletDAO;
    private JDBCPatientInfoDAO patientDAO;
    private User currentUser;

    /**
     * init's DAO connections
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
            try{
                orderDAO = new JDBCOrderDAO();
                tabletDAO = new JDBCTabletDAO();
                patientDAO = new JDBCPatientInfoDAO();

            }catch (NamingException e){
                e.printStackTrace();
            }catch (SQLException e){
                e.printStackTrace();
            }
    }

    /**
     * processes url's for showing tablet info and managing order payment type
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        currentUser = (User)req.getSession().getAttribute(USER);
        String action = req.getParameter("action");
        Tablet tablet = null;
        try {
            if(action == null) {
                tablet = tabletDAO.getTabletById(Integer.parseInt(req.getParameter("tabletId")));
                List<Order> orders = orderDAO.select();

                req.setAttribute("orders", orders);
                req.setAttribute(USER, currentUser);
                req.setAttribute("tablet", tablet);
                req.getRequestDispatcher(TABLET_INFO).forward(req, resp);
            } else if(req.getSession().getAttribute("orders") == null){
                resp.sendRedirect(BUCKET_URL);
            }else if(action.equalsIgnoreCase(REGISTERED_ACTION)){
                int sum = 0;
                for (Order o : orderDAO.selectForPatient(currentUser.getId()))
                    sum += o.getTablet().getPrice() * o.getPackageCount();

                req.setAttribute("patient", patientDAO.getPatientByUserId(currentUser.getId()));
                req.setAttribute("order_price", sum);
                req.getRequestDispatcher(ORDER_CONFIRM_REGISTERED).forward(req, resp);
            }
        } catch (EmailException | OutOfRangeException e) {
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * processes url's for incrementing packages count in bucket order, for managing payment type for order, for showing
     * the result of order transaction
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("In DoPost");
        String action = req.getParameter("action");
        currentUser = (User)req.getSession().getAttribute(USER);
        String orderPrice = req.getParameter("order_price");
        try {
            req.setAttribute("patient", patientDAO.getPatientByUserId(currentUser.getId()));

            req.setAttribute("order_price", orderPrice);

            if (action.equals(UPDATE_COUNTER)) {
                Order selectedOrder = orderDAO.getOrderById(Integer.parseInt(req.getParameter("order_id")));
                String jsonData = null;
                try {
                    Lock lock = new ReentrantLock();
                    lock.lock();

                    selectedOrder.setPackageCount(Integer.parseInt(req.getParameter("pack_count")));
                    orderDAO.update(selectedOrder);
                    lock.unlock();

                    resp.setContentType("application/json; charset=UTF-8");
                    PrintWriter out = resp.getWriter();
                    jsonData = "{ " +
                                            "\"pack_count\" : " + selectedOrder.getPackageCount() +
                                            ", \"order_sum\" : " + countSum() + " }";

                    out.print(jsonData);
                } catch (OutOfRangeException | NumberFormatException e) {
                    e.printStackTrace();
                    resp.setContentType("application/json; charset=UTF-8");
                    jsonData = "{ \"pack_count\" : " + selectedOrder.getPackageCount() + ", \"order_sum\" : " + countSum() + " }";
                    log.info(jsonData);
                    resp.getWriter().print(jsonData);
                }
            } else if (action.equals(ORDER_CONFIRM)) {
                String paymentType = req.getParameter("hidden_payment_type");
                String orderPrc = req.getParameter("hidden_price");

                payMoneyForOrder(paymentType, Double.parseDouble(orderPrc));
                req.setAttribute("patient", patientDAO.getPatientByUserId(currentUser.getId()));
                req.setAttribute(PAYMENT_TYPE, paymentType);
                req.getSession().removeAttribute("orders");
                req.getRequestDispatcher(ORDER_SUCCES).forward(req, resp);

            } else if (action.equals(REGISTERED_ACTION)) {
                req.getRequestDispatcher(ORDER_CONFIRM_REGISTERED).forward(req, resp);
            }
        } catch (EmailException | OutOfRangeException e) {
            e.printStackTrace();
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * count's final price for patient order
     * @return
     * @throws OutOfRangeException
     */
    private int countSum() throws OutOfRangeException {
        int sum = 0;
        List<Order> orders = orderDAO.selectForPatient(currentUser.getId());
        for (Order o : orders){
            sum+= o.getPackageCount() * o.getTablet().getPrice();
        }
        return sum;
    }

    /**
     * consuming patient's money from his account
     * @param paymentType
     * @param orderFullPrice
     * @throws OutOfRangeException
     * @throws EmailException
     */
    private void payMoneyForOrder(String paymentType, double orderFullPrice) throws EmailException, OutOfRangeException{
        Lock lock = new ReentrantLock();
        lock.lock();

        Patient patient = patientDAO.getPatientByUserId(currentUser.getId());

        if (paymentType.equals("cash"))
            patient.setVirtualCash(patient.getVirtualCash() - orderFullPrice);
        else if (paymentType.equals("card"))
            patient.setCreditCardCash(patient.getCreditCardCash() - orderFullPrice);

        orderDAO.deleteForPatient(currentUser.getId());
        patientDAO.update(patient);

        lock.unlock();
    }
}
