package by.epam.controller.pharmasist.profile;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.User;
import by.epam.model.users.pharmasists.JDBCPharmasistDAO;
import by.epam.model.users.pharmasists.Pharmasist;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 20.06.2016.
 */

@WebServlet("/pharmasist/profile")
public class ProfileController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(ProfileController.class));
    /**
     * stores directory for pharmasist profile page
     */
    private static final String PHARMASIST_PROFILE_PAGE = "view/account/profile.jsp";
    /**
     * stores url to pharmasist main menu
     */
    private static final String PHARMASIST_MENU_URL = "/pharmasist/tablets";

    private JDBCPharmasistDAO pharmasistDAO;

    /**
     * init's DAO's
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            pharmasistDAO = new JDBCPharmasistDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    /**
     * processes url for changing pharmasist profile info
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("profileController get Method");
        User user = (User) req.getSession().getAttribute("user");
        Pharmasist pharm = null;
        try {
            pharm = pharmasistDAO.getPharmasistByUserId(user.getId());
            req.setAttribute("pharmasist", pharm);
        req.getRequestDispatcher(PHARMASIST_PROFILE_PAGE).forward(req, resp);
        } catch (EmailException | OutOfRangeException e) {
            e.printStackTrace();
            resp.getWriter().write(e.toString());
        }
    }
    /**
     * updates pharmasist info after clicking submit
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("In ProfileController post method");

        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String telephone = req.getParameter("telephone");
        String eMail = req.getParameter("e-mail");
        String adress = req.getParameter("adress");
        String id = req.getParameter("pharmasist_id");
        String pharmacyOccupation = req.getParameter("pharmacy_occupation");
        String age = req.getParameter("age");
        log.info("Values: " + name + surname + telephone + eMail + adress);

        try {
            Pharmasist pharmasist = new Pharmasist();
            pharmasist.setId(Integer.parseInt(id));
            pharmasist.setUserId(Integer.parseInt(id));
            pharmasist.setName(name);
            pharmasist.setSurname(surname);
            pharmasist.setAdress(adress);
            pharmasist.setTelephone(telephone);
            pharmasist.seteMail(eMail);
            pharmasist.setRoleId(3);
            pharmasist.setPharmacyOccupation(pharmacyOccupation);
            pharmasist.setAge(Integer.parseInt(age));

            Lock lock = new ReentrantLock();
            lock.lock();
            pharmasistDAO.update(pharmasist);
            lock.unlock();
            resp.sendRedirect(PHARMASIST_MENU_URL);
        }catch (EmailException | OutOfRangeException e){
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
}
