package by.epam.controller.pharmasist.tablets;

import by.epam.model.tablets.Tablet;
import by.epam.model.tablets.dao.JDBCTabletDAO;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.tablets.types.TabletType;
import by.epam.model.tablets.types.TypeFactory;
import by.epam.model.users.users.User;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 15.06.2016.
 */
@WebServlet("/pharmasist/tablets")
public class TabletListController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(TabletListController.class));

    private JDBCTabletDAO tabletDAO;
    /**
     * stored directory for list of tablets for pharmasist
     */
    private static final String LIST_TABLET = "view/product_management/tablets.jsp";
    /**
     * stores insert or edit menu's
     */
    private static final String INSERT_OR_EDIT = "view/product_management/tablet.jsp";

    /**
     * init's DAO's
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            tabletDAO = new JDBCTabletDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * processes url's for interacting with tablets with pharmasist privilegies(e.g. INSERT, UPDATE, DELETE)
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        User user = (User)req.getSession().getAttribute("user"); //getting user
        String forward = "";
        try {
            if(action != null && action.equalsIgnoreCase("delete")){
                forward = LIST_TABLET;
                int tabletId = Integer.parseInt(req.getParameter("tablet_id"));
                tabletDAO.delete(tabletId);
                req.setAttribute("tablets", tabletDAO.select());
            }
            else if(action != null && action.equalsIgnoreCase("edit")){
                forward = INSERT_OR_EDIT;
                int tabletId = Integer.parseInt(req.getParameter("tablet_id"));
                Tablet tablet = tabletDAO.getTabletById(tabletId);

                req.setAttribute("types", TabletType.values());
                req.setAttribute("tablet", tablet);
            }
            else if(action != null && action.equalsIgnoreCase("insert")){
                req.setAttribute("types", TabletType.values());
                forward = INSERT_OR_EDIT;
            }else {
                forward = LIST_TABLET;
                req.setAttribute("tablets", tabletDAO.select());
            }

            req.setAttribute("pharmasist", user);
            req.getRequestDispatcher(forward).forward(req, resp);
        } catch (OutOfRangeException e) {
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * processes url's for inserting and updating tablets
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Tablet tablet = new Tablet();

        boolean needRecepie;
        if(req.getParameter("need_recepie") == null)
            needRecepie = false;
        else
            needRecepie = true;

        try {
            tablet.setName(req.getParameter("name"));
            tablet.setPrice(Double.parseDouble(req.getParameter("price")));
            tablet.setNeedRecepie(needRecepie);
            tablet.setTypeId(Integer.parseInt(req.getParameter("types")));
            tablet.setTabletType(TypeFactory.getType(tablet.getTypeId()));
            tablet.setDescription(req.getParameter("description"));
            tablet.setWeight(Double.parseDouble(req.getParameter("weight")));
            tablet.setPillsCount(Integer.parseInt(req.getParameter("pills_count")));
            String tabletId = req.getParameter("tablet_id");
            Lock lock = new ReentrantLock();
            lock.lock();

            if (tabletId == null || tabletId.isEmpty())
                tabletDAO.insert(tablet);
            else {
                tablet.setTabletId((Integer.parseInt(tabletId)));
                tabletDAO.update(tablet);
            }

            lock.unlock();

            RequestDispatcher view = req.getRequestDispatcher(LIST_TABLET);
            req.setAttribute("tablets", tabletDAO.select());
            view.forward(req, resp);
        } catch (OutOfRangeException e){
            e.printStackTrace();
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
}
