package by.epam.controller.doctor;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.ordercalls.OrderCall;
import by.epam.model.orders.ordercalls.dao.JDBCOrderCallDAO;
import by.epam.model.orders.orders.Order;
import by.epam.model.orders.orders.dao.JDBCOrderDAO;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 31.05.2016.
 */
@WebServlet("/doctor/accept_recepie")
public class AcceptRecepieController extends HttpServlet {
    /**
     * page directory for confirmed call info
     */
    private static final String CONFIRM_CALL_PAGE = "view/recepie_call/call_confirmed.jsp";

    /**
     * dao object for order
     */
    private JDBCOrderDAO orderDAO;
    /**
     * dao object for order call
     */
    private JDBCOrderCallDAO orderCallDAO;

    @Override
    public void init() throws ServletException {
        try{
            orderDAO = new JDBCOrderDAO();
            orderCallDAO = new JDBCOrderCallDAO();
        }catch (SQLException e){
            e.printStackTrace();
        }catch (NamingException e){
            e.printStackTrace();
        }
    }

    /**
     * allows confirmed order, deletes order call, and then forwarding request to {@value #CONFIRM_CALL_PAGE}
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int orderId = Integer.parseInt(req.getParameter("idOrder"));
        User user = (User)req.getSession().getAttribute("user");
        try {
            Lock lock = new ReentrantLock();
            lock.lock();

            allowOrder(orderId);                //Allowing Order

            OrderCall call = deleteCall(orderId);  //Deleting order call

            lock.unlock();

            req.setAttribute("call", call);
            req.setAttribute("patient", user);
            req.getRequestDispatcher(CONFIRM_CALL_PAGE).forward(req, resp);
        } catch (SQLException | EmailException | OutOfRangeException e) {
            e.printStackTrace();
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * Delets confirmed call from orderCalls table
     * @param orderId
     * @return deleted OrderCall
     * @throws SQLException
     * @throws OutOfRangeException
     * @throws EmailException
     */
    private OrderCall deleteCall(int orderId) throws SQLException, EmailException, OutOfRangeException {
        OrderCall call = null;
        List<OrderCall> orderCalls = orderCallDAO.select();
        for (OrderCall orderCall : orderCalls){
            if(orderCall.getOrderId() == orderId){
                call = orderCall;
                orderCallDAO.delete(orderCall);
                break;
            }
        }
        return call;
    }

    /**
     * Iterates through orders to find allowed order and set it isAllowed to true
     * @param orderId
     * @throws SQLException
     * @throws OutOfRangeException
     */
    private void allowOrder(int orderId) throws SQLException, OutOfRangeException {
        List<Order> orders = orderDAO.select();

        Iterator<Order> it = orders.iterator();
        while (it.hasNext()){
            Order order = it.next();
            if(order.getOrderId() == orderId){
                order.setIsAllowed(true);
                orderDAO.update(order);
            }
        }
    }
}
