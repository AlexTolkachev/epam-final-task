package by.epam.controller.doctor;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.dao.JDBCUserDAO;
import by.epam.model.users.doctors.Doctor;
import by.epam.model.users.doctors.JDBCDoctorDAO;
import by.epam.model.users.doctors.qualifications.Qualification;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 30.05.2016.
 */
@WebServlet("/doctor/account")
public class AccountController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    /**
     * value for storing login page directory
     */
    private static final String LOGIN_PAGE = "view/account/login.jsp";
    /**
     * value for storing patient home menu url
     */
    private static final String PATIENT_MENU = "/patient/tabletList";
    /**
     * value for storing doctor menu url
     */
    private static final String DOCTOR_MENU = "/doctor/order_calls";
    /**
     * value for storing pharmasist menu url
     */
    private static final String PHARMASIST_MENU = "/pharmasist/tablets";
    /**
     * represents doctors role id constant
     */
    private static final int ROLE_ID = 2;

    private static final Logger log = Logger.getLogger(String.valueOf(AccountController.class));

    /**
     * users DAO object
     */
    private JDBCUserDAO userDAO;

    /**
     * doctor info DAO object
     */
    private JDBCDoctorDAO doctorInfoDAO;

    /**
     * inits DAO connections
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            userDAO = new JDBCUserDAO();
            doctorInfoDAO = new JDBCDoctorDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Processes url's for managing doctor's account. (e.g. log in and log out with {@value #LOGIN_PAGE})
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("In Get Controller");
        req.setAttribute("qualifications", Qualification.values());
        HttpSession session = req.getSession();
        session.removeAttribute("user");
        req.getRequestDispatcher(LOGIN_PAGE).forward(req, resp);
    }

    /**
     * Processes urls for user authentification and registration
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        List<User> users = null;
        String action = req.getParameter("action");

        boolean isFound = false;
        try {
            users = userDAO.select();

            if(action == null || action.isEmpty()) {
                for (User u : users) {
                    if (username.equalsIgnoreCase(u.getUsername()) && password.equalsIgnoreCase(u.getPassword())) {
                        log.info("found user!");
                        isFound = true;
                        HttpSession session = req.getSession();
                        session.setAttribute("user", u);
                        if (u.getRoleId() == 1)
                            resp.sendRedirect(PATIENT_MENU);
                        else if (u.getRoleId() == 2)
                            resp.sendRedirect(DOCTOR_MENU);
                        else if (u.getRoleId() == 3)
                            resp.sendRedirect(PHARMASIST_MENU);
                    }
                }
                if (!isFound) {
                    log.info("Found NOONE! for username:" + username + " and password:" + password);
                    req.setAttribute("message", "Account's Invalid");
                    req.getRequestDispatcher(LOGIN_PAGE).forward(req, resp);
                }
            }else if(action.equalsIgnoreCase("register")) {
                Doctor doctor = new Doctor(req.getParameter("name"),
                        req.getParameter("surname"), req.getParameter("username_register"),
                            req.getParameter("password_register"), req.getParameter("adress"), req.getParameter("telephone"),
                                Integer.parseInt(req.getParameter("age")), req.getParameter("e-mail"),
                                    ROLE_ID, Integer.parseInt(req.getParameter("experience")),
                                        Integer.parseInt(req.getParameter("qualification")));

                boolean multipleUsernames = false;
                for (User u : userDAO.select()){
                    if(doctor.getUsername().equalsIgnoreCase(u.getUsername())){
                        req.setAttribute("message", "User with this username already exists!");
                        req.getRequestDispatcher(LOGIN_PAGE).forward(req, resp);
                        multipleUsernames = true;
                    }
                }
                if(!multipleUsernames) {


                    int lastId = doctorInfoDAO.insert(doctor);
                    doctor.setId(lastId);
                    doctor.setUserId(lastId);
                    HttpSession session = req.getSession();
                    session.setAttribute("user", doctor);
                    resp.sendRedirect(DOCTOR_MENU);
                }
            }
        }catch (EmailException | OutOfRangeException e){
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
}
