package by.epam.controller.doctor;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.doctors.Doctor;
import by.epam.model.users.doctors.JDBCDoctorDAO;
import by.epam.model.users.doctors.qualifications.Qualification;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 20.06.2016.
 */

@WebServlet("/doctor/profile")
public class ProfileController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    /**
     * value for doctor profile page directory
     */
    private static final String PROFILE_PAGE = "view/account/profile.jsp";
    /**
     * value for doctors main menu url
     */
    private static final String ORDER_CALLS_URL = "/doctor/order_calls";
    private static final Logger log = Logger.getLogger(String.valueOf(ProfileController.class));

    /**
     * doctorDAO object
     */
    private JDBCDoctorDAO doctorDAO;

    /**
     * init's DAO connections
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            doctorDAO = new JDBCDoctorDAO();
        }catch (NamingException e){
            e.printStackTrace();
        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    /**
     * processes url for changing doctor profile info
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("profileController get Method");
        User user = (User) req.getSession().getAttribute("user");
        Doctor doctor = null;
        try {
            doctor = doctorDAO.getDoctorByUserId(user.getId());
            req.setAttribute("doctor", doctor);
            req.setAttribute("qualifications", Qualification.values());
            req.getRequestDispatcher(PROFILE_PAGE).forward(req, resp);
        } catch (OutOfRangeException | EmailException e) {
            e.printStackTrace();
            resp.getWriter().write(e.toString());
        }
    }

    /**
     * updates doctor info after clicking submit
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("In ProfileController post method");

        try {
            Doctor doctor = new Doctor();
            doctor.setId(Integer.parseInt(req.getParameter("doctor_id")));
            doctor.setName(req.getParameter("name"));
            doctor.setSurname(req.getParameter("surname"));
            doctor.setAdress(req.getParameter("adress"));
            doctor.setTelephone(req.getParameter("telephone"));
            doctor.seteMail(req.getParameter("e-mail"));
            doctor.setRoleId(2);
            doctor.setPassword(req.getParameter("password_register"));
            doctor.setUserId(Integer.parseInt(req.getParameter("doctor_id")));
            doctor.setExperience(Integer.parseInt(req.getParameter("experience")));
            doctor.setQualificationId(Integer.parseInt(req.getParameter("qualification")));
            doctor.setAge(Integer.parseInt(req.getParameter("age")));

            Lock lock = new ReentrantLock();
            lock.lock();

            log.info("doctor_id: " + doctor.getId() + " user_id: " + doctor.getUserId());
            doctorDAO.update(doctor);

            lock.unlock();

            resp.sendRedirect(ORDER_CALLS_URL);
        }catch (EmailException | OutOfRangeException e){
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
}
