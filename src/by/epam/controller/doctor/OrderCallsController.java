package by.epam.controller.doctor;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.ordercalls.OrderCall;
import by.epam.model.orders.ordercalls.dao.JDBCOrderCallDAO;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 14.05.2016.
 */

@WebServlet("/doctor/order_calls")
public class OrderCallsController extends HttpServlet {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }
    private static final Logger log = Logger.getLogger(String.valueOf(OrderCallsController.class));
    /**
     * value for storing order calls page directory
     */
    private static final String ORDER_CALLS_PAGE = "view/recepie_call/order_calls.jsp";

    /**
     * storing OrderCallDAO object
     */
    private JDBCOrderCallDAO orderCallDAO;

    /**
     * init's dao connections
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        try{
            orderCallDAO = new JDBCOrderCallDAO();
        }catch (SQLException e){
            e.printStackTrace();
        }catch (NamingException e){
            e.printStackTrace();
        }
    }

    /**
     * Processes urls for showing list of doctor's received order calls
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("Doctor Controller before select");
        User user = (User)req.getSession().getAttribute("user");

        List<OrderCall> orderCalls = null;
        try {
            orderCalls = orderCallDAO.selectByDoctorId(user.getId());
            req.setAttribute("calls", orderCalls);
            req.getRequestDispatcher(ORDER_CALLS_PAGE).forward(req, resp);
        } catch (EmailException | OutOfRangeException e) {
            log.error(e);
            resp.getWriter().write(e.toString());
        }
    }
}
