package by.epam.model.users.users;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.exceptions.EmailException;

/**
 * Created by User on 15.06.2016.
 */
public class User {
    /**
     * int representation of user id
     */
    private int id;
    /**
     * string representation of user login
     */
    private String username;
    /**
     * string represenatation of user password
     */
    private String password;
    /**
     * string representation of firstname
     */
    private String name;
    /**
     * string representation of surname
     */
    private String surname;
    /**
     * string represenation of adress
     */
    private String adress;
    /**
     * string representation of telephone
     */
    private String telephone;
    /**
     * int representation of afe
     */
    private int age;
    /**
     * string representation of email
     */
    private String eMail;
    /**
     * int representation of role id. role id has three possible values(1-for patient, 2-for doctor, 3-for pharmasist)
     */
    private int roleId;
    /**
     * string regex for matching inputed email
     */
    private static final String EMAIL_REGEX = "(\\w{3,})@(\\w+\\.)(\\w{2,4})";

    /**
     * Creates user instance without parameters
     */
    public User() {}

    /**
     *Creates the instance of user with parameters
     * @param name this is String representation of firstname
     * @param surname this is String representation of secondname
     * @param username this is String representation of user login
     * @param password this is String representation of user password
     * @param adress this is String representation of user adress
     * @param telephone this is String representation of telephone
     * @param age this is String representation of user age
     * @param eMail this is String representation of user e-Mail
     * @param roleId role id has three possible values(1-for patient, 2-for doctor, 3-for pharmasist)
     * @throws OutOfRangeException thrown when age is less than 1 or more than 150
     * @throws EmailException thrown when e-mail don't pass pattern matcher
     */


    public User(String name, String surname, String username, String password, String adress,
                String telephone, int age, String eMail, int roleId) throws OutOfRangeException, EmailException {
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setAdress(adress);
        setTelephone(telephone);
        setAge(age);
        seteMail(eMail);
        setRoleId(roleId);
    }

    /**
     *
     * @return roleId int representation of user role has three possible values(1-for patient, 2-for doctor, 3-for pharmasist)
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     *
     * @param roleId to set (int)
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    /**
     *
     * @return string representation of eMail
     */
    public String geteMail() {
        return eMail;
    }

    /**
     *
     * @param eMail string representation of eMail
     * @throws EmailException when eMail doesn't matches pattern
     */
    public void seteMail(String eMail) throws EmailException{
        if(!eMail.matches(EMAIL_REGEX))
            throw new EmailException("Wrong E-mail adress: " + eMail);
        this.eMail = eMail;
    }

    /**
     *
     * @return int representation of age
     */
    public int getAge() {
        return age;
    }

    /**
     *
     * @param age must be greater than 0 and not greater than 100
     * @throws OutOfRangeException
     */
    public void setAge(int age) throws OutOfRangeException {
        if (age >=100 || age <= 0)
            throw new OutOfRangeException("Wrong age: " + age);

        this.age = age;
    }

    /**
     *
     * @return string representation of user telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone telephone to set (string)
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return string representation of adress
     */
    public String getAdress() {
        return adress;
    }

    /**
     *
     * @param adress adress to set(string)
     */
    public void setAdress(String adress) {
        this.adress = adress;
    }

    /**
     *
     * @return int representation of user id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id id to set(int)
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return string representation of name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name name to set(string)
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return string representation of surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     *
     * @param surname surname to set(string)
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return string representation of password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password password to set(string)
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return string representation of username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username username to set(string)
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
