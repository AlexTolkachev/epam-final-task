package by.epam.model.users.users.exceptions;

/**
 * Created by User on 20.06.2016.
 */
public class EmailException extends Exception {
    /**
     * constructor for setting the exception message. Called when e-mail doesn't matches pattern
     * @param message exception message to set
     */
    public EmailException(String message){ super(message);}
}
