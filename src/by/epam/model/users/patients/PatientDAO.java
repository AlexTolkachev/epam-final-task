package by.epam.model.users.patients;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.List;

/**
 * Created by User on 08.05.2016.
 */
public interface PatientDAO {
    /**
     * inserts new row in users table and in patient-info table
     * @param patient
     */
    int insert(Patient patient);

    /**
     * returns all rows from patient-info
     * @return list of patient objects(List<Patient>)
     * @throws OutOfRangeException thrown when virtual cash is less than 0 or more than 10000000
     * @throws EmailException thrown when e-mail does not match pattern
     */
    List<Patient> select() throws OutOfRangeException, EmailException;

    /**
     * updates a row in users table and in patient info table
     * @param patient holds the information of patient to be updated
     */
    void update(Patient patient);

    /**
     * gets patient info from users table and patient-info table by it's user id
     * @param userId int representation of id for matching a row in table
     * @return Patient object
     * @throws OutOfRangeException thrown when virtual cash is less than 0 or more than 10000000
     * @throws EmailException thrown when e-mail does not match pattern
     */
    Patient getPatientByUserId(int userId) throws OutOfRangeException, EmailException;
}
