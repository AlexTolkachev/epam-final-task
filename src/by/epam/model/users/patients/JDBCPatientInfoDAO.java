package by.epam.model.users.patients;

import by.epam.model.dao.DbUtil;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 08.05.2016.
 */
public class JDBCPatientInfoDAO implements PatientDAO{
    static {
        new DOMConfigurator().doConfigure("D:\\EPAM Training\\Final Task\\EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(JDBCPatientInfoDAO.class));
    /**
     * connection for access to jdbc transactions and methods
     */
    private Connection connection;
    /**
     * dataSource for binding java object with sql database
     */
    private DataSource dataSource;

    /**
     * constructor for getting connection that will allow to interact with database tables
     * @throws NamingException
     * @throws SQLException
     */
    public JDBCPatientInfoDAO() throws NamingException, SQLException{
        Context envCtx = new InitialContext();
        dataSource = (DataSource)envCtx.lookup("java:comp/env/jdbc/pharmacy");
        connection = dataSource.getConnection();
    }

    @Override
    public int insert(Patient patient) {
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement1 = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int lastId = 0;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT id FROM users ORDER BY id DESC LIMIT 1");
            while (resultSet.next()){
                lastId = resultSet.getInt(1);
                lastId++;
            }

            preparedStatement = connection.prepareStatement("INSERT INTO users(name, surname, adress," +
                    "telephone, age, username, password, e_mail, id, role_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1, patient.getName());
            preparedStatement.setString(2, patient.getSurname());
            preparedStatement.setString(3, patient.getAdress());
            preparedStatement.setString(4, patient.getTelephone());
            preparedStatement.setInt(5, patient.getAge());
            preparedStatement.setString(6, patient.getUsername());
            preparedStatement.setString(7, patient.getPassword());
            preparedStatement.setString(8, patient.geteMail());
            preparedStatement.setInt(9, lastId);
            preparedStatement.setInt(10, patient.getRoleId());

            preparedStatement.executeUpdate();

            preparedStatement1 =
                    connection.prepareStatement("INSERT INTO pharmacy.patient_info (physical_group, virtual_cash, " +
                            "virtual_credit_card_cash, user_id) VALUES (?, ?, ?, ?)");
            preparedStatement1.setInt(1, patient.getPhysicalGroup());
            preparedStatement1.setDouble(2, patient.getVirtualCash());
            preparedStatement1.setDouble(3, patient.getCreditCardCash());
            preparedStatement1.setInt(4, lastId);
            preparedStatement1.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            lastId = 0;
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
            DbUtil.close(preparedStatement1);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return lastId;
    }

    @Override
    public Patient getPatientByUserId(int userId) throws OutOfRangeException, EmailException {
        PreparedStatement preparedStatement = null;
        Patient patient = null;
        ResultSet resultSet = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT name, surname, physical_group," +
                    "adress, e_mail, telephone, virtual_cash, virtual_credit_card_cash, username, password, age, role_id, user_id " +
                    "FROM pharmacy.patient_info " +
                    "INNER JOIN users ON user_id=users.id WHERE user_id=?");
            preparedStatement.setInt(1, userId);

            resultSet = preparedStatement.executeQuery();
            patient = new Patient();
            log.info("JDBC Patient DAO. Show list of patients");
            while(resultSet.next()){
                patient = new Patient();
                patient.setId(resultSet.getInt("user_id"));
                patient.setName(resultSet.getString("name"));
                patient.setSurname(resultSet.getString("surname"));
                patient.setPhysicalGroup(resultSet.getInt("physical_group"));
                patient.setAdress(resultSet.getString("adress"));
                patient.seteMail(resultSet.getString("e_mail"));
                patient.setTelephone(resultSet.getString("telephone"));
                patient.setVirtualCash(resultSet.getDouble("virtual_cash"));
                patient.setCreditCardCash(resultSet.getDouble("virtual_credit_card_cash"));
                patient.setUsername(resultSet.getString("username"));
                patient.setPassword(resultSet.getString("password"));
                patient.setAge(resultSet.getInt("age"));
                patient.setRoleId(resultSet.getInt("role_id"));
                patient.setUserId(resultSet.getInt("user_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
        return patient;
    }

    //etc.....

    @Override
    public void update(Patient patient) {
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            log.info("Update patient. Creating statement");
            preparedStatement = connection.prepareStatement("UPDATE users SET telephone=?, adress=?, " +
                    "e_mail=?, age=?, name=?, surname=?, role_id=? WHERE id=?;");
            preparedStatement.setString(1, patient.getTelephone());
            preparedStatement.setString(2, patient.getAdress());
            preparedStatement.setString(3, patient.geteMail());
            preparedStatement.setInt(4, patient.getAge());
            preparedStatement.setString(5, patient.getName());
            preparedStatement.setString(6, patient.getSurname());
            preparedStatement.setInt(7, patient.getRoleId());
            preparedStatement.setInt(8, patient.getUserId());
            preparedStatement.executeUpdate();

            preparedStatement =
                    connection.prepareStatement("UPDATE patient_info SET physical_group=?, virtual_cash=?," +
                            "virtual_credit_card_cash=? WHERE user_id=?;");
            preparedStatement.setInt(1, patient.getPhysicalGroup());
            preparedStatement.setDouble(2, patient.getVirtualCash());
            preparedStatement.setDouble(3, patient.getCreditCardCash());
            preparedStatement.setInt(4, patient.getUserId());

            log.info("Update patient. Values: " + patient.getName() + patient.getSurname() + patient.getVirtualCash() + patient.getAdress() + patient.geteMail() + patient.getTelephone() + patient.getUsername());
            log.info("Update patient. Executing query");
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public List<Patient> select() throws OutOfRangeException, EmailException {
        List<Patient> patients = new ArrayList<>();
        log.info("Creating connection");
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            log.info("Creating statement");

            if(connection.isClosed())
                connection = dataSource.getConnection();

            statement = connection.createStatement();
            log.info("Executing query");
            resultSet = statement.executeQuery("SELECT name, surname, physical_group," +
                    "adress, e_mail, telephone, virtual_cash, virtual_credit_card_cash, username, password, age, role_id, user_id " +
                    "FROM pharmacy.patient_info " +
                    "INNER JOIN users ON user_id=users.id");

            Patient patient = null;
            log.info("Show list of patients");
            while(resultSet.next()){
                patient = new Patient();
                patient.setId(resultSet.getInt("user_id"));
                patient.setName(resultSet.getString("name"));
                patient.setSurname(resultSet.getString("surname"));
                patient.setPhysicalGroup(resultSet.getInt("physical_group"));
                patient.setAdress(resultSet.getString("adress"));
                patient.seteMail(resultSet.getString("e_mail"));
                patient.setTelephone(resultSet.getString("telephone"));
                patient.setVirtualCash(resultSet.getDouble("virtual_cash"));
                patient.setCreditCardCash(resultSet.getDouble("virtual_credit_card_cash"));
                patient.setUsername(resultSet.getString("username"));
                patient.setPassword(resultSet.getString("password"));
                patient.setAge(resultSet.getInt("age"));
                patient.setRoleId(resultSet.getInt("role_id"));
                patient.setUserId(resultSet.getInt("user_id"));
                patients.add(patient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }

        return patients;
    }
}
