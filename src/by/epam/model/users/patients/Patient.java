package by.epam.model.users.patients;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;

/**
 * Created by User on 08.05.2016.
 */
public class Patient extends User{
    /**
     * string representation of patients physical group(could be 1,2 or 3)
     */
    private int physicalGroup;
    /**
     * double representation of patients online cash
     */
    private double virtualCash;
    /**
     * double representation of patients creditCardCash
     */
    private double creditCardCash;
    /**
     * int representation of user id for binding patient info with users table by id
     */
    private int userId;

    /**
     * empty constructor for patient
     */
    public Patient() {}

    /**
     *
     * @param username username to set(string)
     * @param password password to set(string)
     * @param name firstanme to set(string)
     * @param surname secondname to set(string)
     * @param adress adress to set(stirng)
     * @param age age to set(int)
     * @param telephone telehpone to set(string)
     * @param eMail e-mail to set(string)
     * @param virtualCash virtualCash to set(string)
     * @param creditCardCash creditCardCash to set(string)
     * @param physicalGroup physicalGroup to set(Enum PhysicalGroup)
     * @param roleId role id to set(int)
     * @return Patient object
     * @throws OutOfRangeException thrown when virtual cash is less than 0 or more than 10000000
     * @throws EmailException thrown when e-mail does not match pattern
     */
    public Patient(String username, String password, String name, String surname, String adress,
                        int age, String telephone, String eMail,
                            double virtualCash, double creditCardCash, int physicalGroup,
                                                int roleId) throws EmailException,
                                                    OutOfRangeException {
        super(name, surname, username, password, adress, telephone, age, eMail, roleId );
        setVirtualCash(virtualCash);
        setCreditCardCash(creditCardCash);
        setPhysicalGroup(physicalGroup);
    }

    /**
     *
     * @return int representation of user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * sets user id
     * @param userId userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     *
     * @return double representation of virtual cash
     */
    public double getVirtualCash() {
        return virtualCash;
    }

    /**
     * sets virtual cash
     * @param virtualCash virtual cash to set(double)
     * @throws OutOfRangeException thrown when virtual cash is less than 0 or more than 1000000000
     */
    public void setVirtualCash(double virtualCash) throws OutOfRangeException{
        if(virtualCash < 0 || virtualCash > 1000000000)
            throw new OutOfRangeException("Wrong virtual cash: " + virtualCash);

        this.virtualCash = virtualCash;
    }

    /**
     *
     * @return double representation of credit card cash
     */
    public double getCreditCardCash() { return creditCardCash;  }

    /**
     * sets credit card cash
     * @param creditCardCash creditCardCash to be set
     * @throws OutOfRangeException thrown when credit cash is less than 0 or more than 10000000000
     */
    public void setCreditCardCash(double creditCardCash) throws OutOfRangeException{
        if(creditCardCash < 0 || creditCardCash > 1000000000)
            throw new OutOfRangeException("Wrong card cash: " + creditCardCash);

        this.creditCardCash = creditCardCash;  }

    /**
     *
     * @return int representation of physical group
     */
    public int getPhysicalGroup() {
        return physicalGroup;
    }

    /**
     * sets the physical group
     * @param physicalGroup physical group to be set
     * @throws OutOfRangeException thrown when physicalGroup is less than 1 or more than 3
     */
    public void setPhysicalGroup(int physicalGroup) throws OutOfRangeException{
        if(physicalGroup < 1 || physicalGroup > 3)
            throw new OutOfRangeException("Wrong physical group: " + physicalGroup);
        this.physicalGroup = physicalGroup;
    }
}
