package by.epam.model.users.pharmasists;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;

/**
 * Created by User on 08.05.2016.
 */
public class Pharmasist extends User{
    /**
     * string representation of pharmacy adress
     */
    private String pharmacyOccupation;
    /**
     * int representation of user id for binding pharmasist info with users table by id
     */
    private int userId;

    /**
     * empty constructor for creation patient
     */
    public Pharmasist() {}

    /**
     * constructor with parameters for creating patient
     * @param username username to set(string)
     * @param password password to set(string)
     * @param name firstanme to set(string)
     * @param surname secondname to set(string)
     * @param adress adress to set(stirng)
     * @param age age to set(int)
     * @param telephone telehpone to set(string)
     * @param eMail e-mail to set(string)
     * @param roleId role id to set(int)
     * @param pharmacyOccupation pharmacyOccupation to set(stirng)
     * @throws EmailException thrown when e-mail is not valid
     */
    public Pharmasist(String name, String surname, String username, String password, String adress,
                      String telephone, int age, String eMail, int roleId, String pharmacyOccupation) throws OutOfRangeException, EmailException {
        super(name, surname, username, password, adress, telephone, age, eMail, roleId);
        this.pharmacyOccupation = pharmacyOccupation;
    }

    /**
     *
     * @return int representation of user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * set user id
     * @param userId user id to be set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     *
     * @return string represenation of pharmacy adress
     */
    public String getPharmacyOccupation() {
        return pharmacyOccupation;
    }

    /**
     * set pharmacy occupation
     * @param pharmacyOccupation pharmacy ocupation to be set
     */
    public void setPharmacyOccupation(String pharmacyOccupation) {
        this.pharmacyOccupation = pharmacyOccupation;
    }
}
