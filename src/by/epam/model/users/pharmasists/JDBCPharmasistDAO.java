package by.epam.model.users.pharmasists;

import by.epam.model.dao.DbUtil;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.exceptions.EmailException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 15.06.2016.
 */
public class JDBCPharmasistDAO implements PharmasistDAO{
    /**
     * dataSource for binding java object with sql database
     */
    private DataSource dataSource;
    /**
     * connection for access to jdbc transactions and methods
     */
    private Connection connection;

    /**
     * constructor for getting connection that will allow to interact with database tables
     * @throws NamingException
     * @throws SQLException
     */
    public JDBCPharmasistDAO() throws NamingException, SQLException{
        Context envCtx = new InitialContext();
        dataSource = (DataSource)envCtx.lookup("java:comp/env/jdbc/pharmacy");
        connection = dataSource.getConnection();

    }

    @Override
    public List<Pharmasist> select() throws OutOfRangeException, EmailException {
        List<Pharmasist> pharmasists = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {

            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM pharmacy.patients");
            Pharmasist pharmasist = null;

            while(resultSet.next()){
                pharmasist = new Pharmasist();
                pharmasist.setId(resultSet.getInt("idPharmasist"));
                pharmasist.setName(resultSet.getString("name"));
                pharmasist.setSurname(resultSet.getString("surname"));
                pharmasist.setAdress(resultSet.getString("adress"));
                pharmasist.seteMail(resultSet.getString("email"));
                pharmasist.setTelephone(resultSet.getString("telephone"));
                pharmasist.setAge(resultSet.getInt("age"));
                pharmasist.setPharmacyOccupation(resultSet.getString("pharmacyOccupation"));
                pharmasist.setUsername(resultSet.getString("username"));
                pharmasist.setPassword(resultSet.getString("password"));

                pharmasists.add(pharmasist);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }

        return pharmasists;
    }

    @Override
    public Pharmasist getPharmasistByUserId(int userId) throws EmailException, OutOfRangeException {
        PreparedStatement preparedStatement = null;
        Pharmasist pharmasist = null;
        ResultSet resultSet = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT name, surname, " +
                    "adress, e_mail, telephone, username, password, age, role_id, user_id, pharmacy_occupation " +
                    "FROM pharmacy.pharmasist_info " +
                    "INNER JOIN users ON user_id=users.id WHERE user_id=?");
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                pharmasist = new Pharmasist();
                pharmasist.setId(resultSet.getInt("user_id"));
                pharmasist.setName(resultSet.getString("name"));
                pharmasist.setSurname(resultSet.getString("surname"));

                pharmasist.setAdress(resultSet.getString("adress"));
                pharmasist.seteMail(resultSet.getString("e_mail"));
                pharmasist.setTelephone(resultSet.getString("telephone"));

                pharmasist.setPharmacyOccupation(resultSet.getString("pharmacy_occupation"));
                pharmasist.setUsername(resultSet.getString("username"));
                pharmasist.setPassword(resultSet.getString("password"));
                pharmasist.setAge(resultSet.getInt("age"));
                pharmasist.setRoleId(resultSet.getInt("role_id"));
                pharmasist.setUserId(resultSet.getInt("user_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
        return pharmasist;
    }

    @Override
    public void update(Pharmasist pharmasist) {
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();

            preparedStatement = connection.prepareStatement("UPDATE users SET " +
                    "telephone=?, adress=?, e_mail=?, age=?, name=?, surname=?, role_id=? WHERE id=?;");

            preparedStatement.setString(1, pharmasist.getTelephone());
            preparedStatement.setString(2, pharmasist.getAdress());
            preparedStatement.setString(3, pharmasist.geteMail());
            preparedStatement.setInt(4, pharmasist.getAge());
            preparedStatement.setString(5, pharmasist.getName());
            preparedStatement.setString(6, pharmasist.getSurname());
            preparedStatement.setInt(7, pharmasist.getRoleId());
            preparedStatement.setInt(8, pharmasist.getId());
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement("UPDATE pharmasist_info SET pharmacy_occupation=? " +
                    "WHERE user_id=?;");
            preparedStatement.setString(1, pharmasist.getPharmacyOccupation());
            preparedStatement.setInt(2, pharmasist.getUserId());

            preparedStatement.executeUpdate();
            preparedStatement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public int insert(Pharmasist pharmasist) {
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement1 = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int lastId = 0;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();

            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT id FROM users ORDER BY id DESC LIMIT 1");
            while (resultSet.next()){
                lastId = resultSet.getInt(1);
                lastId++;
            }

            preparedStatement = connection.prepareStatement("INSERT INTO users(name, surname, adress," +
                    "telephone, age, username, password, e_mail, id, role_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1, pharmasist.getName());
            preparedStatement.setString(2, pharmasist.getSurname());
            preparedStatement.setString(3, pharmasist.getAdress());
            preparedStatement.setString(4, pharmasist.getTelephone());
            preparedStatement.setInt(5, pharmasist.getAge());
            preparedStatement.setString(6, pharmasist.getUsername());
            preparedStatement.setString(7, pharmasist.getPassword());
            preparedStatement.setString(8, pharmasist.geteMail());
            preparedStatement.setInt(9, lastId);
            preparedStatement.setInt(10, pharmasist.getRoleId());

            preparedStatement.executeUpdate();
            pharmasist.setId(lastId);
            pharmasist.setUserId(lastId);

            preparedStatement1 =
                    connection.prepareStatement("INSERT INTO pharmacy.pharmasist_info (pharmacy_occupation, user_id) " +
                            "VALUES (?, ?)");
            preparedStatement1.setString(1, pharmasist.getPharmacyOccupation());
            preparedStatement1.setInt(2, lastId);
            preparedStatement1.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            lastId = 0;
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
            DbUtil.close(preparedStatement1);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return lastId;
    }
}
