package by.epam.model.users.pharmasists;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.List;

/**
 * Created by User on 15.06.2016.
 */
public interface PharmasistDAO {
    /**
     * returns all Pharmasists from database
     * @return list of Pharmasist objects
     * @throws EmailException when e-mail is not valid
     * @throws OutOfRangeException when age is less than 0 or more than 100
     */
    List<Pharmasist> select() throws OutOfRangeException, EmailException;

    /**
     * inserts new row in users table and in pharmasists table
     * @param pharmasist holds the information to be set in row
     */
    void update(Pharmasist pharmasist);

    /**
     *
     * @param userId for matching row in table
     * @return returns pharmasist object specified with user id
     * @throws EmailException when e-mail is not valid
     * @throws OutOfRangeException when age is less than 0 or more than 100
     */
    Pharmasist getPharmasistByUserId(int userId) throws EmailException, OutOfRangeException;

    /**
     * inserts new row in users table and pharmasist-info table binded with user id
     * @param pharmasist
     */
    int insert(Pharmasist pharmasist);
}
