package by.epam.model.users.doctors;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.List;

/**
 * Created by User on 14.05.2016.
 */
public interface DoctorDAO {
    /**
     * inserts a new row in doctors table
     * @param doctor holds the information to be set in row
     */
    int insert(Doctor doctor);

    /**
     * selects all rows from doctors table
     * @return list of Doctors
     * @throws OutOfRangeException thrown when age column value is less than 0 or more than 100
     * @throws EmailException thrown when e-mail column value doe not match e-mail pattern
     */
    List<Doctor> select() throws OutOfRangeException, EmailException;

    /**
     * selects doctors by specified qualification id
     * @param qualificationId int representation of qualification id for matching a row in table
     * @return list of Doctors
     * @throws OutOfRangeException thrown when age column value is less than 0 or more than 100
     * @throws EmailException thrown when e-mail column value doe not match e-mail pattern
     */
    List<Doctor> selectDoctorsByQualification(int qualificationId) throws OutOfRangeException, EmailException;

    /**
     * gets doctor object from sql table
     * @param userId int representation of id for matching a row in table
     * @return doctor object
     * @throws EmailException thrown when e-mail column value doe not match e-mail pattern
     */
    Doctor getDoctorByUserId(int userId) throws OutOfRangeException, EmailException;

    /**
     * updates doctors info in users table and in doctor-info table
     * @param doctor holds values to be updated
     */
    void update(Doctor doctor);
}
