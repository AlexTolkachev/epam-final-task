package by.epam.model.users.doctors.qualifications;

/**
 * Created by User on 20.06.2016.
 */
public enum Qualification {
    /**
     * enums for specifying doctor's qualification
     */
    OTHOLARINGOLOGIST, DENTIST, THERAPIST, SURGERY, OPHTALMOLOGIST;

    /**
     *
     * @return string representation of qualification type
     */
    public String getName() { return name(); }
}
