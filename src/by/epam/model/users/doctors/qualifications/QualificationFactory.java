package by.epam.model.users.doctors.qualifications;

/**
 * Created by User on 24.06.2016.
 */
public class QualificationFactory {
    public static Qualification getQualificationByName(String name) {
        if(name != null) {
            name = name.toUpperCase();

            switch (name) {
                case "DENTIST":
                    return Qualification.DENTIST;
                case "OPHTALMOLOGIST":
                    return Qualification.OPHTALMOLOGIST;
                case "SURGERY":
                    return Qualification.SURGERY;
                case "OTHOLARINGOLOGIST":
                    return Qualification.OTHOLARINGOLOGIST;
                case "THERAPIST":
                    return Qualification.THERAPIST;
            }
        }
        return null;
    }
}
