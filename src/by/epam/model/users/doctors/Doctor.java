package by.epam.model.users.doctors;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.ordercalls.OrderCall;
import by.epam.model.users.doctors.qualifications.Qualification;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.Collections;
import java.util.List;

/**
 * Created by User on 08.05.2016.
 */
public class Doctor extends User {
    /**
     * id for binding doctor-info with user id in users table
     */
    private int userId;
    /**
     * int representation of doctor experience(in years). Shows working experience of a specific doctor
     */
    private int experience;
    /**
     * id for binding with doctor qualification
     */
    private int qualificationId;
    /**
     * enum represenation of doctor's qualification. Show's his working specialisation
     */
    private Qualification qualification;
    /**
     * list of user defined objects that holds doctor's recepie calls
     */
    private List<OrderCall> recepieCalls;

    /**
     * empty constructor for creation doctors object
     */
    public Doctor() {}

    /**
     * constructor for specifying Doctor's variables
     * @param name represents doctors firstname(string)
     * @param surname represents doctors secondname(string)
     * @param username represents doctors login(string)
     * @param password represents doctors password(string)
     * @param adress represents doctors adress(string)
     * @param telephone string representation of doctors telephone
     * @param age int representation of doctors age
     * @param eMail string represenation of e-mail
     * @param roleId int representation of user role. (1-for patient, 2-for doctor, 3-for pharmasist)
     * @param experience int representation of doctors working experience(in years)
     * @param qualificationId id for binding with doctor qualification
     * @throws EmailException thrown when email doesn't match e-mail pattern
     * @throws OutOfRangeException thrown when doctors working experience less than 0 or more than 100
     */
    public Doctor(String name, String surname, String username, String password, String adress,
                      String telephone, int age, String eMail, int roleId, int experience, int qualificationId) throws EmailException, OutOfRangeException {
        super(name, surname, username, password, adress, telephone, age, eMail, roleId);
        setExperience(experience);
        setQualificationId(qualificationId);
    }

    /**
     *
     * @return int representation of user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     *
     * @param userId sets the user id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     *
     * @return int representation of doctors working experience
     */
    public int getExperience() {
        return experience;
    }

    /**
     * sets doctors working experience
     * @param experience int representation of doctors working experience
     * @throws OutOfRangeException thrown when experiecne less than 0 or more than 100
     */
    public void setExperience(int experience) throws OutOfRangeException {
        if(experience < 0 || experience >= 100)
            throw new OutOfRangeException("Wrong experience! " + experience);

        this.experience = experience;
    }

    /**
     *
     * @return int representation of qualification id
     */
    public int getQualificationId() {
        return qualificationId;
    }

    /**
     *
     * @param qualificationId int representation of qualification id
     * @throws OutOfRangeException thrown when qualification id more than the count of doctors qualifications or less than 1
     */
    public void setQualificationId(int qualificationId)throws OutOfRangeException{
        if(qualificationId > Qualification.values().length || qualificationId < 1)
            throw new OutOfRangeException("Wrong qualification: " + qualificationId);

        this.qualificationId = qualificationId;
    }

    /**
     *
     * @return enum representation of doctors qualification
     */
    public Qualification getQualification() {
        return qualification;
    }

    /**
     *
     * @param qualification qualification to set (Qualification type)
     */
    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    /**
     *
     * @return list of doctors recepie calls
     */
    public List<OrderCall> getRecepieCalls() {
        return Collections.unmodifiableList(recepieCalls);
    }

    /**
     *
     * @param recepieCalls recepieCalls to set(List<OrderCall>)
     */
    public void setRecepieCalls(List<OrderCall> recepieCalls) { this.recepieCalls = recepieCalls;  }
}
