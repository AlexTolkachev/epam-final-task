package by.epam.model.users.dao;

import by.epam.model.dao.DbUtil;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 16.06.2016.
 */
public class JDBCUserDAO implements UserDAO{
    static {
        new DOMConfigurator().doConfigure("D:\\EPAM Training\\Final Task\\EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(JDBCUserDAO.class));
    /**
     * connection for access to jdbc transactions and methods
     */
    private Connection connection;
    /**
     * dataSource for binding java object with sql database
     */
    private DataSource dataSource;

    /**
     * constructor for getting connection that will allow to interact with database tables
     * @throws NamingException
     * @throws SQLException
     */
    public JDBCUserDAO() throws NamingException, SQLException{
        Context envCtx = new InitialContext();
        dataSource = (DataSource)envCtx.lookup("java:comp/env/jdbc/pharmacy");
        connection = dataSource.getConnection();
    }

    /**
     * implementation of insert method that allows to insert new row to User table
     * @param user a java object that holds information for a row
     */
    @Override
    public void insert(User user) {
        PreparedStatement preparedStatement = null;

        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("INSERT INTO users(name, surname, adress," +
                    "telephone, age, username, password, e_mail) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getAdress());
            preparedStatement.setString(4, user.getTelephone());
            preparedStatement.setInt(5, user.getAge());
            preparedStatement.setString(6, user.geteMail());
            preparedStatement.setString(7, user.getUsername());
            preparedStatement.setString(8, user.getPassword());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    /**
     * implementation of select method that allows us to get all rows of user table
     * @return list of user defined objects(e.g List<User>)
     * @throws EmailException when e-mail isnt valid
     * @throws OutOfRangeException when age is less than 0 or more than 100
     */
    @Override
    public List<User> select() throws EmailException, OutOfRangeException {
        Statement statememnt = null;
        ResultSet resultSet = null;
        List<User> users = new ArrayList<>();
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();

            statememnt = connection.createStatement();
            log.info("executing query");
            resultSet = statememnt.executeQuery("SELECT * FROM users");
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setTelephone(resultSet.getString("telephone"));
                user.setAdress(resultSet.getString("adress"));
                user.seteMail(resultSet.getString("e_mail"));
                user.setAge(resultSet.getInt("age"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setRoleId(resultSet.getInt("role_id"));

                users.add(user);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return users;
    }

    /**
     * implementation of update method that allows to update specified row
     * @param user holds the information for finding and updating a row in user table
     */
    @Override
    public void update(User user) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE users SET username=?, password=?," +
                    "telephone=?, adress=?, e_mail=?, age=?, name=?, surname=?, role_id=? WHERE id=?;");
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getTelephone());
            preparedStatement.setString(4, user.getAdress());
            preparedStatement.setString(5, user.geteMail());
            preparedStatement.setInt(6, user.getAge());
            preparedStatement.setString(7, user.getName());
            preparedStatement.setString(8, user.getSurname());
            preparedStatement.setInt(9, user.getRoleId());
            preparedStatement.setInt(10, user.getId());
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }
}
