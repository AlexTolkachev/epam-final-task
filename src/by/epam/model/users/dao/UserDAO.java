package by.epam.model.users.dao;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.users.users.User;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.List;

/**
 * Created by User on 16.06.2016.
 */
public interface UserDAO {
    /**
     * inserts new row in users table
     * @param user holds information for a new row
     */
    void insert(User user);

    /**
     * updates a row in users table
     * @param user holds the information for finding row and updating it's information
     */
    void update(User user);

    /**
     * selects all rows in users table
     * @return list of user defined objects (e.g List<User>)
     * @throws EmailException thrown when e-mail isn't correct
     * @throws OutOfRangeException thrown when age is less than 0 or more than 100
     */
    List<User> select()  throws EmailException, OutOfRangeException;
}
