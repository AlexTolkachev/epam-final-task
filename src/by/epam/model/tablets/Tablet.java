package by.epam.model.tablets;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.tablets.types.TabletType;

/**
 * Created by User on 08.05.2016.
 */
public class Tablet {
    /**
     * int representation of tablet id
     */
    private int tabletId;
    /**
     * string representation of tablet name
     */
    private String name;
    /**
     * show if tablet needs a recepie
     */
    private boolean needRecepie;
    /**
     * represents a type of tablet
     */
    private TabletType tabletType;
    /**
     * used for binding tabletType with Tablet
     */
    private int typeId;
    /**
     * additional information about a tablet
     */
    private String description;
    /**
     * double representation of tablets price
     */
    private double price;
    /**
     * double representation of package weight
     */
    private double weight;
    /**
     * int representation of pills count in package
     */
    private int pillsCount;

    /**
     *
     * @return int representation of {@link #pillsCount}
     */
    public int getPillsCount() {
        return pillsCount;
    }

    /**
     * sets {@link #pillsCount}
     * @param pillsCount pillsCount to set(int)
     */
    public void setPillsCount(int pillsCount) throws OutOfRangeException {
        if(pillsCount < 1 || pillsCount >= 1000)
            throw new OutOfRangeException("Pills count is out of range: " + pillsCount);
        this.pillsCount = pillsCount;
    }

    /**
     *
     * @return double representation of {@link #weight}
     */
    public double getWeight() {
        return weight;
    }

    /**
     * setes {@link #weight}
     * @param weight weight to set(double)
     */
    public void setWeight(double weight) throws OutOfRangeException {
        if(weight < 1 || weight > 100000)
            throw new OutOfRangeException("Weight is out of range: " + weight);
        this.weight = weight;
    }
    /**
     *
     * @return {@link #tabletType} binded with Tablet
     */
    public TabletType getTabletType() {
        return tabletType;
    }

    /**
     * sets {@link #tabletType}
     * @param tabletType tabletType to set
     */
    public void setTabletType(TabletType tabletType) {
        this.tabletType = tabletType;
    }

    /**
     *
     * @return string representation of {@link #description}
     */
    public String getDescription() {
        return description;
    }

    /**
     * set's the description
     * @param description {@link #description} to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return int representation of type id
     */
    public int getTypeId() {
        return typeId;
    }

    /**
     * set's the type id
     * @param typeId {@link #typeId} to set
     * @throws OutOfRangeException thrown when type id is less than 1 or bigger than count of tablet types
     */
    public void setTypeId(int typeId) throws OutOfRangeException{
        if(typeId < 1 || typeId > TabletType.values().length)
            throw new OutOfRangeException("Wrong tablet typeId: " + typeId);

        this.typeId = typeId;
    }

    /**
     *
     * @return doublet representation of {@link #price}
     */
    public double getPrice() {
        return price;
    }

    /**
     * set's {@link #price}
     * @param price {@link #price} to set
     * @throws OutOfRangeException thrown when price is less than 1 or bigger than 10000000
     */
    public void setPrice(double price)throws OutOfRangeException{
        if(price < 1 || price > 10000000)
            throw new OutOfRangeException("Wrong tablet price:" + price);

        this.price = price;
    }

    /**
     *
     * @return int representation of {@link #tabletId}
     */
    public int getTabletId() {
        return tabletId;
    }

    /**
     * set's the {@link #tabletId}
     * @param tabletId {@link #tabletId} to set
     */
    public void setTabletId(int tabletId) {
        this.tabletId = tabletId;
    }

    /**
     *
     * @return string representation of {@link #name}
     */
    public String getName() {
        return name;
    }

    /**
     * sets {@link #name}
     * @param name {@link #name} to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return boolean representation of {@link #needRecepie}
     */
    public boolean isNeedRecepie() {
        return needRecepie;
    }

    /**
     * sets {@link #needRecepie}
     * @param needRecepie {@link #needRecepie} to set
     */
    public void setNeedRecepie(boolean needRecepie) {
        this.needRecepie = needRecepie;
    }

    /**
     *
     * @return same hash-code for equal {@link #name}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + name.hashCode();
        return result;
    }

    /**
     *
     * @param obj
     * @return true if only {@link #name} values are equal
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(!getClass().equals(obj.getClass()))
            return false;
        Tablet o = (Tablet)obj;
        if(!this.getName().equalsIgnoreCase(o.getName()))
            return false;
        return true;
    }
}
