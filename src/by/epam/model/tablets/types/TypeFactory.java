package by.epam.model.tablets.types;

/**
 * Created by User on 17.06.2016.
 */
public class TypeFactory {
    /**
     * gest TabletType by it's id
     * @param typeId type id to set
     * @return TabletType Enum value
     */
    public static TabletType getType(int typeId){
        TabletType tabletType = null;
        switch (typeId){
            case 1: tabletType = TabletType.THROAT; break;
            case 2: tabletType = TabletType.EAR; break;
            case 3: tabletType = TabletType.NASAL; break;
            case 4: tabletType = TabletType.TOOTH; break;
            case 5: tabletType = TabletType.STOMACH; break;
            case 6: tabletType = TabletType.BONES; break;
            case 7: tabletType = TabletType.EYES; break;
        }
        return tabletType;
    }
}
