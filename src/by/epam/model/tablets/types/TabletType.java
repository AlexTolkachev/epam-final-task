package by.epam.model.tablets.types;

/**
 * Created by User on 17.06.2016.
 */
public enum TabletType {
    /**
     * Enum values for TabletType
     */
    THROAT(1, "tablet_type.enum.throat"), EAR(2, "tablet_type.enum.ear"), NASAL(3, "tablet_type.enum.nasal"), TOOTH(4, "tablet_type.enum.tooth"),
        STOMACH(5, "tablet_type.enum.stomach"), BONES(6, "tablet_type.enum.bones"), EYES(7, "tablet_type.enum.eyes");

    /**
     * represent's doctor's qualification id to bind with a type
     */
    private int typeId;

    /**
     *
     * @return String representation of message code
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * internationalization code for enum value
     */
    private String messageCode;

    /**
     *
     * @return int representation of typeId
     */
    public int getTypeId() {
        return typeId;
    }
    /**
     *
     * @param typeId int representation of qualification id
     */
    TabletType(int typeId, String messageCode){
        this.messageCode = messageCode;
        this.typeId = typeId;
    }

    /**
     *
     * @return string representation of Enum value
     */
    public String getName() {
        return name();
    }


}
