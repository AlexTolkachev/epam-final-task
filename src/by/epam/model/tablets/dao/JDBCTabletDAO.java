package by.epam.model.tablets.dao;

import by.epam.model.dao.DbUtil;
import by.epam.model.tablets.Tablet;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.tablets.types.TypeFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 14.05.2016.
 */
public class JDBCTabletDAO implements TabletDAO {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(JDBCTabletDAO.class));
    /**
     * connection for interacting with sql
     */
    private Connection connection;
    /**
     * data source for specified schema
     */
    private DataSource dataSource;
    /**
     * number of record's to be selected
     */
    private int noOfRecords;

    /**
     * gets the connection from connection pool
     * @throws NamingException
     * @throws SQLException
     */
    public JDBCTabletDAO() throws NamingException, SQLException{
        Context envCtx = new InitialContext();
        dataSource = (DataSource)envCtx.lookup("java:comp/env/jdbc/pharmacy");
        connection = dataSource.getConnection();
    }

    public int getNoOfRecords(){
        return noOfRecords;
    }

    @Override
    public void delete(int tabletId) {
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("DELETE FROM tablets WHERE idTablet=?");
            preparedStatement.setInt(1, tabletId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public void update(Tablet tablet) {
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("UPDATE tablets SET name=?," +
                    "price=?, type_id=?, description=?, need_recipe=?, weight_of_pack=?, pills_count=? WHERE idTablet=?;");
            preparedStatement.setString(1, tablet.getName());
            preparedStatement.setDouble(2, tablet.getPrice());
            preparedStatement.setInt(3, tablet.getTypeId());
            preparedStatement.setString(4, tablet.getDescription());
            preparedStatement.setBoolean(5, tablet.isNeedRecepie());
            preparedStatement.setDouble(6, tablet.getWeight());
            preparedStatement.setInt(7, tablet.getPillsCount());
            preparedStatement.setInt(8, tablet.getTabletId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public int getQualificationByTabletType(int tabletTypeId) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int qualificationId = 0;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT qualification_id FROM tablets_types WHERE id=? limit 1;");
            preparedStatement.setInt(1, tabletTypeId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                qualificationId = resultSet.getInt("qualification_id");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
        }

        return qualificationId;
    }

    @Override
    public void insert(Tablet tablet) {
        PreparedStatement preparedStatement = null;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("INSERT INTO pharmacy.tablets (name, need_recipe, price, type_id, description, weight_of_pack, pills_count) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, tablet.getName());
            preparedStatement.setBoolean(2, tablet.isNeedRecepie());
            preparedStatement.setDouble(3, tablet.getPrice());
            preparedStatement.setInt(4, tablet.getTypeId());
            preparedStatement.setString(5, tablet.getDescription());
            preparedStatement.setDouble(6, tablet.getWeight());
            preparedStatement.setInt(7, tablet.getPillsCount());

            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public Set<Tablet> viewAllTabletsByType(int offset, int noOfRecords, int typeId) throws OutOfRangeException {
        Set<Tablet> tabletSet = new HashSet<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Tablet tablet = null;
        String query = "select SQL_CALC_FOUND_ROWS * from tablets WHERE type_id=" + typeId + " limit " + offset + ", " + noOfRecords;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                tablet = new Tablet();
                tablet.setTabletId(resultSet.getInt("idTablet"));
                tablet.setName(resultSet.getString("name"));
                tablet.setNeedRecepie(resultSet.getBoolean("need_recipe"));
                tablet.setPrice(resultSet.getDouble("price"));
                tablet.setTypeId(resultSet.getInt("type_id"));
                tablet.setDescription(resultSet.getString("description"));
                tablet.setTabletType(TypeFactory.getType(tablet.getTypeId()));
                tablet.setWeight(resultSet.getDouble("weight_of_pack"));
                tablet.setPillsCount(resultSet.getInt("pills_count"));
                tabletSet.add(tablet);
            }
            resultSet.close();
            resultSet = statement.executeQuery("SELECT FOUND_ROWS()");
            if(resultSet.next())
                this.noOfRecords = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return tabletSet;
    }

    @Override
    public Set<Tablet> viewAllTablets(int offset, int noOfRecords) throws OutOfRangeException {
        Set<Tablet> tabletSet = new HashSet<>();
        Statement statement = null;
        ResultSet resultSet = null;
        Tablet tablet = null;
        String query = "select SQL_CALC_FOUND_ROWS * from tablets limit " + offset + ", " + noOfRecords;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                tablet = new Tablet();
                tablet.setTabletId(resultSet.getInt("idTablet"));
                tablet.setName(resultSet.getString("name"));
                tablet.setNeedRecepie(resultSet.getBoolean("need_recipe"));
                tablet.setPrice(resultSet.getDouble("price"));
                tablet.setTypeId(resultSet.getInt("type_id"));
                tablet.setDescription(resultSet.getString("description"));
                tablet.setTabletType(TypeFactory.getType(tablet.getTypeId()));
                tablet.setWeight(resultSet.getDouble("weight_of_pack"));
                tablet.setPillsCount(resultSet.getInt("pills_count"));
                tabletSet.add(tablet);
            }
            resultSet.close();
            resultSet = statement.executeQuery("SELECT FOUND_ROWS()");
            if(resultSet.next())
                this.noOfRecords = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return tabletSet;
    }

    @Override
    public Set<Tablet> select() throws OutOfRangeException {
        Set<Tablet> tablets = new HashSet<>();
        log.info("Creating connection");
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();
            log.info("Creating statement");
            statement = connection.createStatement();
            log.info("Executing query");
            resultSet = statement.executeQuery("SELECT * FROM pharmacy.tablets;");

            Tablet tablet = null;
            log.info("Show list of tablets");
            while(resultSet.next()){
                tablet = new Tablet();
                tablet.setTabletId(resultSet.getInt("idTablet"));
                tablet.setName(resultSet.getString("name"));
                tablet.setNeedRecepie(resultSet.getBoolean("need_recipe"));
                tablet.setPrice(resultSet.getDouble("price"));
                tablet.setTypeId(resultSet.getInt("type_id"));
                tablet.setDescription(resultSet.getString("description"));
                tablet.setTabletType(TypeFactory.getType(tablet.getTypeId()));
                tablet.setWeight(resultSet.getDouble("weight_of_pack"));
                tablet.setPillsCount(resultSet.getInt("pills_count"));
                tablets.add(tablet);
            }
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }

        return tablets;
    }

    @Override
    public Tablet getTabletById(int tabletId) throws OutOfRangeException {
        PreparedStatement preparedStatement = null;
        Tablet tablet = null;
        ResultSet resultSet = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM pharmacy.tablets WHERE tablets.idTablet = ?;");
            preparedStatement.setInt(1, tabletId);

            resultSet = preparedStatement.executeQuery();
            tablet = new Tablet();
            while (resultSet.next()){
                tablet.setTabletId(resultSet.getInt("idTablet"));
                tablet.setName(resultSet.getString("name"));
                tablet.setNeedRecepie(resultSet.getBoolean("need_recipe"));
                tablet.setPrice(resultSet.getDouble("price"));
                tablet.setTypeId(resultSet.getInt("type_id"));
                tablet.setDescription(resultSet.getString("description"));
                tablet.setTabletType(TypeFactory.getType(tablet.getTypeId()));
                tablet.setWeight(resultSet.getDouble("weight_of_pack"));
                tablet.setPillsCount(resultSet.getInt("pills_count"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DbUtil.close(resultSet);
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
        return tablet;
    }
}
