package by.epam.model.tablets.dao;

import by.epam.model.tablets.Tablet;
import by.epam.model.exceptions.OutOfRangeException;

import java.sql.SQLException;
import java.util.Set;

/**
 * Created by User on 14.05.2016.
 */
public interface TabletDAO {
    /**
     * inserts row in tablet
     * @param tablet tablet to insert
     */
    void insert(Tablet tablet);

    /**
     * selects all rows from Tablets table
     * @return list of tablets
     * @throws SQLException
     * @throws OutOfRangeException
     */
    Set<Tablet> select() throws SQLException, OutOfRangeException;

    /**
     * get's tablet by it's id
     * @param tabletId for matching row in Tablets table
     * @return user defined class Tablet
     * @throws OutOfRangeException
     */
    Tablet getTabletById(int tabletId) throws OutOfRangeException;

    /**
     * return's doctor's qualification id for tablet type id
     * @param tabletTypeId matching row in table
     * @return int representation of qualification id
     */
    int getQualificationByTabletType(int tabletTypeId);

    /**
     * deletes tablet by it's id
     * @param tabletId matching row in table
     */
    void delete(int tabletId);

    /**
     * updates row in tablets table
     * @param tablet matching tablet to update
     */
    void update(Tablet tablet);

    /**
     * view specific amount of tablets
     * @param offset number of a row from what to return a records
     * @param noOfRecords number of records to return
     * @return  set of tablets
     * @throws OutOfRangeException
     */
    Set<Tablet> viewAllTablets(int offset, int noOfRecords) throws OutOfRangeException;

    /**
     * view specific amount of tablets with specified tablet type
     * @param offset
     * @param offset number of a row from what to return a records
     * @param noOfRecords number of records to return
     * @return
     * @throws OutOfRangeException
     */
    Set<Tablet> viewAllTabletsByType(int offset, int noOfRecords, int typeId) throws OutOfRangeException;
}
