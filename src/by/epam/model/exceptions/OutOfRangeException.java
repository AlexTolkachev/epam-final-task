package by.epam.model.exceptions;

/**
 * Created by User on 26.06.2016.
 */
public class OutOfRangeException extends Exception {
    public OutOfRangeException(String message) {super(message);}
}
