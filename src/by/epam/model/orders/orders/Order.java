package by.epam.model.orders.orders;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.tablets.Tablet;
import by.epam.model.users.patients.Patient;

/**
 * Created by User on 30.05.2016.
 */
public class Order {
    /**
     * int representation of order id
     */
    private int orderId;
    /**
     * int represenntation of package count in order
     */
    private int packageCount;
    /**
     * double representation of 1 package weight
     */
    private double weight;

    /**
     * int representation of patient id
     */
    private int patientId;
    /**
     * patient object binded with order
     */
    private Patient patient;
    /**
     * specifies if order is allowed for purchasing
     */
    private boolean isAllowed;
    /**
     * tablet in order
     */
    private Tablet tablet;
    /**
     * binding tablet with oder
     */
    private int tabletId;

    public Order() {}

    public Order(int orderId, int packageCount, int patientId, boolean isAllowed, int tabletId) throws OutOfRangeException {
        setOrderId(orderId);
        setPackageCount(packageCount);
        setPatientId(patientId);
        setIsAllowed(isAllowed);
        setTabletId(tabletId);
    }

    /**
     *
     * @return int representation of order id
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * set's order id
     * @param orderId orderId to set(int)
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return int representation of package count
     */
    public int getPackageCount() {
        return packageCount;
    }

    /**
     *
     * @param packageCount packageCount to set(int)
     * @throws OutOfRangeException thrown when packageCount less than 1 or more than 100
     */
    public void setPackageCount(int packageCount) throws OutOfRangeException{
        if(packageCount < 1 || packageCount > 100)
            throw new OutOfRangeException("Package count is out of range: " + packageCount);

        this.packageCount = packageCount;
    }

    /**
     *
     * @return int representation of patient id
     */
    public int getPatientId() {
        return patientId;
    }

    /**
     * set's patient id
     * @param patientId patientId to set(int)
     */
    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    /**
     *
     * @return patient binded with order
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     *
     * @param patient patient to set(Patient)
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    /**
     *
     * @return boolean representation of {@link #isAllowed}
     */
    public boolean getIsAllowed() {
        return isAllowed;
    }

    /**
     * sets {@link #isAllowed}
     * @param isAllowed {@link #isAllowed} to set
     */
    public void setIsAllowed(boolean isAllowed) {
        this.isAllowed = isAllowed;
    }

    /**
     *
     * @return Tablet representation of {@link #tablet}
     */
    public Tablet getTablet() {
        return tablet;
    }

    /**
     * sets the {@link #tablet}
     * @param tablet {@link #tablet} to set(Tablet)
     */
    public void setTablet(Tablet tablet) {
        this.tablet = tablet;
    }

    /**
     *
     * @return int representation of {@link #tabletId}
     */
    public int getTabletId() {
        return tabletId;
    }

    /**
     * set's talbet id
     * @param tabletId {@link #tabletId} to set(int)
     */
    public void setTabletId(int tabletId) {
        this.tabletId = tabletId;
    }
}
