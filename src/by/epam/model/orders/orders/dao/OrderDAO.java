package by.epam.model.orders.orders.dao;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.orders.Order;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.List;

/**
 * Created by User on 30.05.2016.
 */
public interface OrderDAO {
    /**
     * inserts new row in order table
     * @param order holds the values to set
     */
    void insert(Order order);

    /**
     * selects all rows from table
     * @return list of orders
     * @throws OutOfRangeException
     */
    List<Order> select() throws OutOfRangeException;

    /**
     * updates order row
     * @param order holds the values to be updated
     */
    void update(Order order);

    /**
     * selects orders for specified patient
     * @param patientId value to match a row
     * @return list of orders
     * @throws OutOfRangeException
     */
    List<Order> selectForPatient(int patientId) throws OutOfRangeException;

    /**
     * delets specified order by order id
     * @param id matches order row to delete(int)
     */
    void delete(int id);

    /**
     * deletes specified order by patient id
     * @param patientId matches order row to delete(int)
     */
    void deleteForPatient(int patientId);

    /**
     *
     * @param orderId matches order row to select(int)
     * @return specific Order object
     * @throws OutOfRangeException
     * @throws EmailException
     */
    Order getOrderById(int orderId) throws OutOfRangeException, EmailException;

    /**
     *
     * @return last inserted id from users table
     */
    int getLastInsertedId();
}
