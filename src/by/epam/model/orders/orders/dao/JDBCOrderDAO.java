package by.epam.model.orders.orders.dao;

import by.epam.model.dao.DbUtil;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.orders.Order;
import by.epam.model.tablets.Tablet;
import by.epam.model.tablets.dao.JDBCTabletDAO;
import by.epam.model.users.patients.JDBCPatientInfoDAO;
import by.epam.model.users.patients.Patient;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 30.05.2016.
 */
public class JDBCOrderDAO implements OrderDAO {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(JDBCOrderDAO.class));
    /**
     * connection for interacting with sql
     */
    private Connection connection;
    /**
     * data source for specified schema
     */
    private DataSource dataSource;

    /**
     * get's the connection from connection pool
     * @throws NamingException
     * @throws SQLException
     */
    public JDBCOrderDAO() throws NamingException, SQLException{
        Context envCtx = new InitialContext();
        dataSource = (DataSource)envCtx.lookup("java:comp/env/jdbc/pharmacy");
        connection = dataSource.getConnection();
    }

    @Override
    public int getLastInsertedId(){
        int id = 0;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT last_insert_id()");
            while (resultSet.next()){
                id = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return id;
    }

    @Override
    public void insert(Order order) {
        PreparedStatement preparedStatement = null;

        log.info("inserting order");
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("INSERT INTO pharmacy.orders (pack_count, patient_id, idTablet, isAllowed) " +
                            "VALUES (?, ?, ?, ?)");
            preparedStatement.setInt(1, order.getPackageCount());
            preparedStatement.setInt(2, order.getPatientId());
            preparedStatement.setInt(3, order.getTabletId());
            preparedStatement.setBoolean(4, order.getIsAllowed());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public Order getOrderById(int orderId) throws EmailException, OutOfRangeException {
        PreparedStatement orderStatement = null;
        ResultSet resultSetOrders = null;
        log.info("get order by id. Preparing");
        Order order = new Order();
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            orderStatement = connection.prepareStatement("select * FROM pharmacy.orders WHERE orders.idOrder = ?");
            orderStatement.setInt(1, orderId);
            resultSetOrders = orderStatement.executeQuery();
            while (resultSetOrders.next()){
                order = new Order(resultSetOrders.getInt("idOrder"), resultSetOrders.getInt("pack_count"),
                        resultSetOrders.getInt("patient_id"), resultSetOrders.getBoolean("isAllowed"),
                        resultSetOrders.getInt("idTablet"));

                Patient patient = new JDBCPatientInfoDAO().getPatientByUserId(order.getPatientId());
                order.setPatient(patient);

                Tablet tablet = new JDBCTabletDAO().getTabletById(order.getTabletId());
                order.setTablet(tablet);
            }
        } catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        } catch (NamingException e){
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(resultSetOrders);
            DbUtil.close(orderStatement);
            DbUtil.close(connection);
        }

        return order;
    }

    @Override
    public void delete(int id) {
        PreparedStatement preparedStatement = null;
        log.info("delete order. executing statement");
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("DELETE FROM pharmacy.orders WHERE idOrder = ?;");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public void deleteForPatient(int patientId) {
        PreparedStatement preparedStatement = null;
        log.info("delete for patient. Executing statement");
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("DELETE FROM orders WHERE patient_id = ?;");
            preparedStatement.setInt(1, patientId);
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }

    }

    @Override
    public void update(Order order) {
        log.info("JDBC Order DAO update");
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("UPDATE pharmacy.orders SET pack_count=?, patient_id=?, " +
                            "idTablet=?, isAllowed=? WHERE idOrder=?;");
            preparedStatement.setInt(1,  order.getPackageCount());
            preparedStatement.setInt(2,  order.getPatientId());
            preparedStatement.setInt(3, order.getTabletId());
            preparedStatement.setBoolean(4, order.getIsAllowed());
            preparedStatement.setInt(5, order.getOrderId());

            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public List<Order> select() throws OutOfRangeException {
        List<Order> orders = new ArrayList<>();
        log.info("Creating connection");
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();

            log.info("Creating statement");
            statement = connection.createStatement();
            log.info("Executing query");
            resultSet = statement.executeQuery("SELECT * FROM pharmacy.orders;");

            Order order = null;
            log.info("Show list of ORDERS");
            while(resultSet.next()){
                order = new Order(resultSet.getInt("idOrder"), resultSet.getInt("pack_count"),
                        resultSet.getInt("patient_id"), resultSet.getBoolean("isAllowed"),
                        resultSet.getInt("idTablet"));

                Tablet t = new JDBCTabletDAO().getTabletById(resultSet.getInt("idTablet"));
                order.setTablet(t);

                orders.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        } catch (NamingException e){
            e.printStackTrace();
            log.error(e);
        } finally{
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }

        return orders;
    }

    @Override
    public List<Order> selectForPatient(int patientId) throws OutOfRangeException {
        List<Order> orders = new ArrayList<>();
        log.info("Creating connection");
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();

            log.info("Creating statement");
            statement = connection.prepareStatement("SELECT * FROM pharmacy.orders WHERE orders.patient_id = ?;");
            statement.setInt(1, patientId);

            log.info("Executing query");
            resultSet = statement.executeQuery();

            Order order = null;
            log.info("Show list of tablets");
            while(resultSet.next()){
                order = new Order(resultSet.getInt("idOrder"), resultSet.getInt("pack_count"),
                        resultSet.getInt("patient_id"), resultSet.getBoolean("isAllowed"),
                        resultSet.getInt("idTablet"));

                order.setTablet(new JDBCTabletDAO().getTabletById(order.getTabletId()));
                orders.add(order);
            }
        }catch (SQLException e){
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
            log.error(e);
        } finally {
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }

        return orders;
    }
}
