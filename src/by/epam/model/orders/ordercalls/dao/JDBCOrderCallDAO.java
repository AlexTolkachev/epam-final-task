package by.epam.model.orders.ordercalls.dao;

import by.epam.model.dao.DbUtil;
import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.ordercalls.OrderCall;
import by.epam.model.orders.orders.Order;
import by.epam.model.orders.orders.dao.JDBCOrderDAO;
import by.epam.model.users.users.exceptions.EmailException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 30.05.2016.
 */
public class JDBCOrderCallDAO implements OrderCallDAO {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    private static final Logger log = Logger.getLogger(String.valueOf(JDBCOrderCallDAO.class));
    /**
     * connection for interacting with sql
     */
    private Connection connection;
    /**
     * data source for specified schema
     */
    private DataSource dataSource;

    /**
     * get's the connection from connection pool
     * @throws NamingException
     * @throws SQLException
     */
    public JDBCOrderCallDAO() throws NamingException, SQLException{
        Context envCtx = new InitialContext();
        dataSource = (DataSource)envCtx.lookup("java:comp/env/jdbc/pharmacy");
        connection = dataSource.getConnection();
    }

    @Override
    public void insert(OrderCall orderCall) {
        PreparedStatement preparedStatement = null;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("INSERT INTO pharmacy.order_calls (dosePerDay, doctor_id, idOrder) VALUES (?, ?, ?)");
            preparedStatement.setString(1, orderCall.getDosePerDay());
            preparedStatement.setInt(2, orderCall.getDoctorId());
            preparedStatement.setInt(3, orderCall.getOrderId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public List<OrderCall> selectByDoctorId(int doctorId) throws EmailException, OutOfRangeException {
        List<OrderCall> orderCalls = new ArrayList<>();
        PreparedStatement orderCallsStatement = null;
        ResultSet resultSetCalls = null;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();
            orderCallsStatement = connection.prepareStatement("select * from pharmacy.order_calls WHERE order_calls.doctor_id = ?");
            orderCallsStatement.setInt(1, doctorId);
            resultSetCalls = orderCallsStatement.executeQuery();

            while (resultSetCalls.next()) {
                OrderCall orderCall = new OrderCall();
                orderCall.setOrderCallId(resultSetCalls.getInt("idCall"));
                orderCall.setDoctorId(resultSetCalls.getInt("doctor_id"));
                orderCall.setDosePerDay(resultSetCalls.getString("dosePerDay"));
                orderCall.setOrderId(resultSetCalls.getInt("idOrder"));

                Order order = new JDBCOrderDAO().getOrderById(orderCall.getOrderId());

                orderCall.setOrder(order);
                orderCalls.add(orderCall);
            }
        }catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        }catch (NamingException e){
            e.printStackTrace();
            log.error(e);
        } finally{
            DbUtil.close(resultSetCalls);
            DbUtil.close(orderCallsStatement);
            DbUtil.close(connection);
        }

        return orderCalls;
    }

    @Override
    public void deleteByOrderId(int orderId) {
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("delete from pharmacy.order_calls WHERE idOrder = ?;");
            preparedStatement.setInt(1, orderId);
            preparedStatement.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public void delete(OrderCall orderCall) {
        PreparedStatement preparedStatement = null;
        try{
            if(connection.isClosed())
                connection = dataSource.getConnection();
            preparedStatement =
                    connection.prepareStatement("delete from pharmacy.order_calls WHERE idCall = ?");
            preparedStatement.setInt(1, orderCall.getOrderCallId());
            preparedStatement.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
            log.error(e);
        }finally {
            DbUtil.close(preparedStatement);
            DbUtil.close(connection);
        }
    }

    @Override
    public List<OrderCall> select() throws EmailException, OutOfRangeException {
        List<OrderCall> orderCalls = new ArrayList<>();
        log.info("Creating connection");
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            if(connection.isClosed())
                connection = dataSource.getConnection();

            log.info("Creating statement");
            statement = connection.createStatement();
            log.info("DAO. Executing query");
            resultSet = statement.executeQuery("SELECT * FROM pharmacy.order_calls;");

            OrderCall orderCall = null;
            while(resultSet.next()){
                orderCall = new OrderCall();
                orderCall.setOrderCallId(resultSet.getInt("idCall"));
                orderCall.setDoctorId(resultSet.getInt("doctor_id"));
                orderCall.setDosePerDay(resultSet.getString("dosePerDay"));
                orderCall.setOrderId(resultSet.getInt("idOrder"));

                Order order = new JDBCOrderDAO().getOrderById(orderCall.getOrderId());
                orderCall.setOrder(order);
                orderCalls.add(orderCall);
            }
        }catch (NamingException e){
            e.printStackTrace();
            log.error(e);
        }catch (SQLException e) {
            e.printStackTrace();
            log.error(e);
        } finally{
            DbUtil.close(resultSet);
            DbUtil.close(statement);
            DbUtil.close(connection);
        }
        return orderCalls;
    }
}
