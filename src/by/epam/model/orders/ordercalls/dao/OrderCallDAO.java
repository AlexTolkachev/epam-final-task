package by.epam.model.orders.ordercalls.dao;

import by.epam.model.exceptions.OutOfRangeException;
import by.epam.model.orders.ordercalls.OrderCall;
import by.epam.model.users.users.exceptions.EmailException;

import java.util.List;

/**
 * Created by User on 30.05.2016.
 */
public interface OrderCallDAO {
    /**
     * inserts new row in orderCalls table
     * @param orderCall holds the values to set
     */
    void insert(OrderCall orderCall);

    /**
     * select's all rows from order calls table
     * @return
     * @throws OutOfRangeException
     * @throws EmailException
     */
    List<OrderCall> select() throws EmailException, OutOfRangeException;

    /**
     * deletes order call from table
     * @param orderCall stores order call info
     */
    void delete(OrderCall orderCall);

    /**
     * deletes order call from table
     * @param orderId value to match which row to delete
     */
    void deleteByOrderId(int orderId);

    /**
     * select's order calls for specified doctor
     * @param doctorId
     * @return
     * @throws OutOfRangeException
     * @throws EmailException
     */
    List<OrderCall> selectByDoctorId(int doctorId) throws EmailException, OutOfRangeException;
}
