package by.epam.model.orders.ordercalls;

import by.epam.model.users.doctors.Doctor;
import by.epam.model.orders.orders.Order;

/**
 * Created by User on 08.05.2016.
 */
public class OrderCall {
    /**
     * int representation of orderCall id
     */
    private int orderCallId;
    /**
     * Order object binded with order call
     */
    private Order order;
    /**
     * int representation of orderId
     */
    private int orderId;
    /**
     * dose of product per day
     */
    private String dosePerDay;
    /**
     * doctor object binded with order call
     */
    private Doctor doctor;
    /**
     * int representation of doctorId for binding with specific doctor
     */
    private int doctorId;

    public OrderCall() {}

    /**
     * constructor with parameters for setting order call properties
     * @param orderId orderId to set(int)
     * @param dosePerDay dosePerDAY to set(int)
     * @param doctorId doctorId to set(int)
     */
    public OrderCall(int orderId, String dosePerDay, int doctorId){
        this.orderId = orderId;
        this.dosePerDay = dosePerDay;
        this.doctorId = doctorId;
    }

    /**
     *
     * @return user defined representation of a doctor
     */
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     * set's doctor
     * @param doctor doctor value to set(Doctor)
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    /**
     *
     * @return user defined represenation of order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * set's order
     * @param order order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     *
     * @return int representation of order id
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * sets order id
     * @param orderId orderId to set(int)
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return int representation of orderCallId
     */
    public int getOrderCallId() {
        return orderCallId;
    }

    /**
     * sets orderCallId
     * @param orderCallId orderCallId to set(int)
     */
    public void setOrderCallId(int orderCallId) {
        this.orderCallId = orderCallId;
    }

    /**
     *
     * @return string representation of dosePerDay
     */
    public String getDosePerDay() {
        return dosePerDay;
    }

    /**
     * set's dose per day
     * @param dosePerDay dosePerDay to set
     */
    public void setDosePerDay(String dosePerDay) {
        this.dosePerDay = dosePerDay;
    }

    /**
     *
     * @return int representation of doctor id
     */
    public int getDoctorId() {
        return doctorId;
    }

    /**
     * set's doctor id
     * @param doctorId doctorId to set(int)
     */
    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }
}
