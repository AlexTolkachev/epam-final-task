package by.epam.customtags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by User on 21.06.2016.
 */

/**/

@SuppressWarnings("serial")
public class MyNavbarTag extends TagSupport {
    private String language;
    private String deliveryLink;
    private String paymentLink;
    private String orderCallLink;
    private String username;

    public void setDeliveryLink(String deliveryLink) {
        this.deliveryLink = deliveryLink;
    }

    public void setPaymentLink(String paymentLink) {
        this.paymentLink = paymentLink;
    }

    public void setOrderCallLink(String orderCallLink) {
        this.orderCallLink = orderCallLink;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * makes customg tag for upper nav
     * @return
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        String enLng = language.equalsIgnoreCase("en") ? "selected" : "";
        String ruLng = language.equalsIgnoreCase("ru") ? "selected" : "";
        String esLng = language.equalsIgnoreCase("es") ? "selected" : "";
        String text = "<div class=\"my-header\">\n" +
                "  <ul class=\"header-normal header\">\n" +
                "    <form action=\"/changeLanguage\">\n" +
                "      <select id=\"language\" name=\"language\" style=\"background-color: #003399; color: #f2f2f2\" onchange=\"submit()\">\n" +
                "        <option value=\"en\" " + enLng + ">English</option>\n" +
                "        <option value=\"ru\" " + ruLng + ">Русский</option>\n" +
                "        <option value=\"es\" " + esLng + ">Español</option>\n" +
                "      </select>\n" +
                "    </form>\n" +
                "    <li><a href=\"/patient/view/info/delivery_info.jsp\">" + deliveryLink +"</a></li>\n" +
                "    <li><a href=\"/patient/view/info/payment_info.jsp\">" + paymentLink + "</a></li>\n" +
                "    <li>telephone: +375293326369 </li>\n" +
                "    <li> <a id=\"askCall\">" + orderCallLink + "</a></li>\n" +
                "    <li><a href=\"/patient/profile\">" +  username + "</a> </li>\n" +
                "  </ul>\n" +
                "</div>";
        try{
            JspWriter out = pageContext.getOut();
            out.println(text);
        }catch (IOException e){
            throw new JspException(e.toString());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return super.doEndTag();
    }
}
