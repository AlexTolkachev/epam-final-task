package by.epam.customtags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by User on 22.06.2016.
 */
public class MyHeaderTag extends TagSupport {
    private String headerLabel;
    private String headerDescription;
    private String homeLink;
    private String bucketLink;
    private String contactsLink;
    private String logoutLink;

    public void setHeaderLabel(String headerLabel) {
        this.headerLabel = headerLabel;
    }

    public void setHeaderDescription(String headerDescription) {
        this.headerDescription = headerDescription;
    }

    public void setHomeLink(String homeLink) {
        this.homeLink = homeLink;
    }

    public void setBucketLink(String bucketLink) {
        this.bucketLink = bucketLink;
    }

    public void setContactsLink(String contactsLink) {
        this.contactsLink = contactsLink;
    }

    public void setLogoutLink(String logoutLink) {
        this.logoutLink = logoutLink;
    }


    /**
     * makes custom tag for header and bottom nav
     * @return
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        String text = "<div class=\"jumbotron\">\n" +
                "  <div class=\"container text-center\">\n" +
                "    <h1 style=\"color: #003399\">" + headerLabel + "</h1>\n" +
                "    <p style=\"color: #b30000\">" + headerDescription + "</p>\n" +
                "  </div>\n" +
                "</div>\n" +
                "<nav>\n" +
                "  <div class=\"my-container\">\n" +
                "    <ul class=\"navcontainer normal\">\n" +
                "      <li><a href=\"/patient/tabletList\">" + homeLink + "</a></li>\n" +
                "      <li><a href=\"/patient/bucket\">" + bucketLink + "</a></li>\n" +
                "      <li><a href=\"/patient/view/info/contacts_info.jsp\">" + contactsLink + "</a></li>\n" +
                "      <li><a href=\"/patient/account\">" + logoutLink + "</a></li>\n" +
                "    </ul>\n" +
                "  </div>\n" +
                "</nav>";
        try{
            JspWriter out = pageContext.getOut();
            out.println(text);
        }catch (IOException e){
            throw new JspException(e.toString());
        }
        return SKIP_BODY;
    }
}
