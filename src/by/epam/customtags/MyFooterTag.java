package by.epam.customtags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by User on 21.06.2016.
 */

@SuppressWarnings("serial")
public class MyFooterTag extends TagSupport {
    private String copyrightText;
    private String getDealsText;
    private String signUpText;

    public void setCopyrightText(String copyrightText) {
        this.copyrightText = copyrightText;
    }

    public void setGetDealsText(String getDealsText) {
        this.getDealsText = getDealsText;
    }

    public void setSignUpText(String signUpText) {
        this.signUpText = signUpText;
    }

    /**
     * makes custom tag for footer
     * @return
     * @throws JspException
     */
    @Override
    public int doStartTag() throws JspException {
        String footerStart = "<footer class='container-fluid text-center'>";
        String copyright = "<p>" + copyrightText + "</p>";
        String form = "<form class='form-inline'>" + getDealsText;
        String input = "<input type='email' class='form-control' size='50' placeholder='Email Address'>";
        String button = "<button type='button' class='btn btn-danger'>" + signUpText + "</button>";

        try{
            JspWriter out = pageContext.getOut();
            out.write(footerStart );
            out.newLine();
            out.write(copyright);
            out.newLine();
            out.write(form);
            out.newLine();
            out.write(input);
            out.newLine();
            out.write(button);
        }catch (IOException e){
            throw new JspException(e.toString());
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        String endTags = "</form></footer>";
        try {
            pageContext.getOut().write(endTags);
        } catch (IOException e) {
            throw new JspTagException(e.toString());
        }
        return EVAL_PAGE;
    }
}
