package by.epam.customtags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by User on 26.06.2016.
 */
@SuppressWarnings("serial")
public class UserInputFields extends TagSupport {
    private String usernameLabel;
    private String passwordLabel;
    private String firstnameLabel;
    private String secondnameLabel;
    private String adressLabel;
    private String ageLabel;
    private String telephoneLabel;

    private String usernameValue = "";
    private String passwordValue = "";
    private String firstnameValue = "";
    private String secondnameValue = "";
    private String adressValue = "";
    private String ageValue = "";
    private String telephoneValue = "";
    private String emailValue = "";

    public void setUsernameValue(String usernameValue) {
        this.usernameValue = usernameValue;
    }

    public void setPasswordValue(String passwordValue) {
        this.passwordValue = passwordValue;
    }

    public void setFirstnameValue(String firstnameValue) {
        this.firstnameValue = firstnameValue;
    }

    public void setSecondnameValue(String secondnameValue) {
        this.secondnameValue = secondnameValue;
    }

    public void setAdressValue(String adressValue) {
        this.adressValue = adressValue;
    }

    public void setAgeValue(String ageValue) {
        this.ageValue = ageValue;
    }

    public void setTelephoneValue(String telephoneValue) {
        this.telephoneValue = telephoneValue;
    }

    public void setUsernameLabel(String usernameLabel) {
        this.usernameLabel = usernameLabel;
    }

    public void setPasswordLabel(String passwordLabel) {
        this.passwordLabel = passwordLabel;
    }

    public void setFirstnameLabel(String firstnameLabel) {
        this.firstnameLabel = firstnameLabel;
    }

    public void setSecondnameLabel(String secondnameLabel) {
        this.secondnameLabel = secondnameLabel;
    }

    public void setAdressLabel(String adressLabel) {
        this.adressLabel = adressLabel;
    }

    public void setAgeLabel(String ageLabel) {
        this.ageLabel = ageLabel;
    }

    public void setTelephoneLabel(String telephoneLabel) {
        this.telephoneLabel = telephoneLabel;
    }

    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }

    @Override
    public int doStartTag() throws JspException {
        String text = "<fieldset>\n" +
                "            <label for=\"username_register\" class=\"form-label\">" + usernameLabel + ": </label>\n" +
                "            <input type=\"text\" value=\"" + usernameValue + "\" name=\"username_register\" id=\"username_register\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label for=\"password_register\" class=\"form-label\">" + passwordLabel + ": </label>\n" +
                "            <input type=\"password\" value=\"" + passwordValue + "\" name=\"password_register\" id=\"password_register\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label class=\"form-label\" for=\"name\">" + firstnameLabel + ": </label>\n" +
                "            <input type=\"text\" value=\"" + firstnameValue + "\" name=\"name\" id=\"name\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label class=\"form-label\" for=\"surname\">" + secondnameLabel + ": </label>\n" +
                "            <input type=\"text\" value=\"" + secondnameValue + "\"name=\"surname\" id=\"surname\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label class=\"form-label\" for=\"adress\">" + adressLabel + ": </label>\n" +
                "            <input type=\"text\" value=\"" + adressValue + "\" name=\"adress\" id=\"adress\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label class=\"form-label\" for=\"age\">" + ageLabel + ": </label>\n" +
                "            <input type=\"number\" value=\"" + ageValue + "\" max=\"120\" min=\"1\" name=\"age\" id=\"age\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label class=\"form-label\" for=\"telephone\">" + telephoneLabel + ": </label>\n" +
                "            <input type=\"number\" value=\"" + telephoneValue + "\" name=\"telephone\" id=\"telephone\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>\n" +
                "\n" +
                "          <fieldset>\n" +
                "            <label class=\"form-label\" for=\"e-mail\">E-mail: </label>\n" +
                "            <input type=\"email\" value=\"" + emailValue + "\" id=\"e-mail\" name=\"e-mail\" class=\"form-control form-input\" required>\n" +
                "          </fieldset>";
        try{
            JspWriter out = pageContext.getOut();
            out.println(text);
        }catch (IOException e){
            throw new JspException(e.toString());
        }
        return SKIP_BODY;
    }
}
