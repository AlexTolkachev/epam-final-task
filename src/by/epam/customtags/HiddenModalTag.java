package by.epam.customtags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by User on 26.06.2016.
 */
public class HiddenModalTag extends TagSupport {
    private String message;
    private String imageSource;
    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int doStartTag() throws JspException {
        String text = "<div id=\"myModal\" class=\"modal\">\n" +
                "  <div class=\"modal-content\">\n" +
                "    <span class=\"close\">x</span>\n" +
                "    <img src=" + imageSource + " align=\"left\"><h3>" + message + "</h3>\n" +
                "  </div>\n" +
                "</div>";
        try{
            JspWriter out = pageContext.getOut();
            out.println(text);
        }catch (IOException e){
            throw new JspException(e.toString());
        }
        return SKIP_BODY;
    }
}
