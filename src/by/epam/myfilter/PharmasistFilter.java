package by.epam.myfilter;

import by.epam.model.users.users.User;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by User on 19.06.2016.
 */
@WebFilter("/PharmasistFilter")
public class PharmasistFilter implements Filter {
    static {
        new DOMConfigurator().doConfigure("EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    /**
     * pattern for matching patient's account url
     */
    private static final String LOGIN_PAGE = "view/account/login.jsp";
    /**
     * pattern for matching css directory url
     */
    private static final String ACCOUNT_PATTERN_URL = "/pharmasist/account";
    /**
     * pattern for matching javascript directory url
     */
    private static final String CSS_PATTERN_URL = "/pharmasist/css";
    /**
     * login page directory
     */
    private static final String JAVASCRIPT_PATTERN_URL = "/pharmasist/javascript";

    private static final Logger log = Logger.getLogger(String.valueOf(PharmasistFilter.class));

    /**
     * checks if the system has authorised pharmasist
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)servletRequest;

        User user = null;
        HttpSession session = req.getSession();
        user = (User)session.getAttribute("user");
        if(user != null)
            if(user.getRoleId() == 3)
                log.info("Login for Pharmasist: " + user.getUsername());
        if(user == null && !req.getRequestURI().endsWith(ACCOUNT_PATTERN_URL) &&
                !req.getRequestURI().contains(CSS_PATTERN_URL) && !req.getRequestURI().contains(JAVASCRIPT_PATTERN_URL)) {
            log.info("Login Failed!");
            req.getRequestDispatcher(LOGIN_PAGE).forward(servletRequest, servletResponse);
        }else if(user != null && user.getRoleId() != 3){
            log.info("You have no access to this menu");
            req.getSession().removeAttribute("user");
            req.getRequestDispatcher(LOGIN_PAGE).forward(servletRequest, servletResponse);
        }else {
            log.info("Patient Filter:Login Successfull!");
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
