package by.epam.myfilter;

import by.epam.model.users.users.User;
import by.epam.model.users.doctors.qualifications.Qualification;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by User on 30.05.2016.
 */
@WebFilter("/DoctorFilter")
public class DoctorFilter implements Filter {
    static {
        new DOMConfigurator().doConfigure("D:\\EPAM Training\\Final Task\\EPAM6 Final Task\\properties\\log4j.xml", org.apache.log4j.LogManager.getLoggerRepository());
    }

    /**
     * pattern for matching doctor's account url
     */
    private static final String ACCOUNT_PATTERN_URL = "/doctor/account";
    /**
     * pattern for matching css directory url
     */
    private static final String CSS_PATTERN_URL = "/doctor/css";
    /**
     * pattern for matching javascript directory url
     */
    private static final String JAVASCRIPT_PATTERN_URL = "/doctor/javascript";
    /**
     * login page directory
     */
    private static final String LOGIN_PAGE = "view/account/login.jsp";

    private static final Logger log = Logger.getLogger(String.valueOf(DoctorFilter.class));

    public DoctorFilter(){ }

    /**
     * launch when user enters url specified in web.xml for DoctorFilter. Checks if there is authorised doctor
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)servletRequest;

        User user = null;
        HttpSession session = req.getSession();
        user = (User)session.getAttribute("user");

        if(user == null && !req.getRequestURI().endsWith(ACCOUNT_PATTERN_URL) &&
                !req.getRequestURI().contains(CSS_PATTERN_URL) && !req.getRequestURI().contains(JAVASCRIPT_PATTERN_URL)) {
            log.info("Doctor Filter:Login Failed!");
            req.setAttribute("qualifications", Qualification.values());
            req.getRequestDispatcher(LOGIN_PAGE).forward(servletRequest, servletResponse);
        }else if(user != null && user.getRoleId() != 2){
            log.info("You have no access to this menu");
            req.getSession().removeAttribute("user");
            req.getRequestDispatcher(LOGIN_PAGE).forward(servletRequest, servletResponse);
        }else {
            filterChain.doFilter(servletRequest, servletResponse);
            log.info("Doctor Filter:Found Successfull!");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
