<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 15.06.2016
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <script type="text/javascript" src="${root}javascript/add_order_to_table.js"></script>
  <script type="text/javascript" src="${root}javascript/add_to_order.js"></script>

  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">


</head>
<body>
<div class="jumbotron">
  <form action="/changeLanguage">
    <select id="language" name="language" style="background-color: #003399; color: #f2f2f2" onchange="submit()">
      <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
      <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
      <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
    </select>
  </form>
  <div class="container text-center">
    <h1 style="color: #003399"><fmt:message key="header.label"></fmt:message></h1>
    <p style="color: #b30000"><fmt:message key="pharmasist.header.info"></fmt:message></p>
  </div>
</div>
<nav>
  <div class="my-header">
    <ul class="header-normal header">
      <li><a href="/pharmasist/tablets"><fmt:message key="nav.home.link"></fmt:message> </a></li>
      <li><a href="/pharmasist/account?action=logout"><fmt:message key="nav.logout.link"></fmt:message> </a></li>
      <li><a href="/pharmasist/profile"> ${user.username} </a> </li>
    </ul>
  </div>
</nav>
  <table class="table">
    <tr>
      <th><fmt:message key="user.profile.name"></fmt:message> </th>
      <th><fmt:message key="tablet.order.need_recepie"></fmt:message> </th>
      <th><fmt:message key="tablet.edit.price"></fmt:message> </th>
      <th><fmt:message key="tablet.edit.tablet_type"></fmt:message> </th>
      <th colspan="2"><a href="/pharmasist/tablets?action=insert"><fmt:message key="tablet.table.insert.link"></fmt:message> </a> </th>
    </tr>
    <c:forEach items="${tablets}" var="tablet">
      <tr>
        <td>${tablet.name}</td>
        <td>${tablet.needRecepie}</td>
        <td>${tablet.price}</td>
        <td>${tablet.tabletType}</td>
        <td><a href="/pharmasist/tablets?action=edit&tablet_id=${tablet.tabletId}"><fmt:message key="tablet.table.update.link"></fmt:message> </a> </td>
        <td><a href="/pharmasist/tablets?action=delete&tablet_id=${tablet.tabletId}"><fmt:message key="tablet.table.delete.link"></fmt:message> </a> </td>
      </tr>
    </c:forEach>
  </table>
</body>
  <ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
