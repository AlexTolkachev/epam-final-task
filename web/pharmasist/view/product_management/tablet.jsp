<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 19.06.2016
  Time: 12:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />

<html lang="${language}">
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/form_input.css">
</head>
<body>
<form action="/changeLanguage">
  <select id="language" name="language" onchange="submit()" style="margin-left: 50px">
    <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
    <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
  </select>
</form>
  <div class="account-status">
    <form action="/pharmasist/tablets" method="post">
      <fieldset>
        <div>
          <input type="hidden" name="tablet_id" value="${tablet.tabletId}">
        </div>
        <div>
          <label class="form-label" for="name"><fmt:message key="user.profile.name"></fmt:message>:</label>
          <input class="form-control form-input" type="text" name="name" id="name" value="<c:out value="${tablet.name}"></c:out> " placeholder="Tablet Name:" required>
        </div>
        <div>
          <label for="price" class="form-label"><fmt:message key="tablet.edit.price"></fmt:message>:</label>
          <input type="number" class="form-control form-input" name="price" id="price" min="1" max="1000000" step="any" value="${tablet.price}" placeholder="Tablet Price:" required>
        </div>
        <div>
          <label for="need_recepie" class="form-label"><fmt:message key="tablet.order.need_recepie"></fmt:message>:</label>
          <input type="checkbox" class="form-control form-input" name="need_recepie" id="need_recepie" ${tablet.needRecepie == true ? 'checked' : ''} >
        </div>
        <div>
          <label for="weight" class="form-label"><fmt:message key="tablet.order.weight"></fmt:message>:</label>
          <input type="number" class="form-control form-input" name="weight" id="weight" value="${tablet.weight}" step="any" min="1" max="10000" placeholder="weight of package" required>
        </div>
        <div>
          <label for="pills_count" class="form-label"><fmt:message key="tablet.order.pills_count"></fmt:message>:</label>
          <input type="number" step="1" min="1" max="1000" class="form-control form-input" name="pills_count" id="pills_count" value="${tablet.pillsCount}" placeholder="Pillcs count" required>
        </div>
        <c:set value="1" var="counter"></c:set>
        <div>
          <label for="types" class="form-label"><fmt:message key="tablet.edit.tablet_type"></fmt:message>:</label>
          <select name="types" id="types" class="form-control form-input">
            <c:forEach items="${types}" var="type">
              <option value="${counter}" ${counter == tablet.typeId ? 'selected="selected"' : ''}>${type}</option>
              <c:set var="counter" value="${counter+1}"></c:set>
            </c:forEach>
          </select>
        </div>
        <div>
          <label for="description"><fmt:message key="tablet.edit.description"></fmt:message>: </label>
          <input type="text" class="form-control form-input" value="${tablet.description}" id="description" name="description" placeholder="Description:" required>
        </div>
        <div>
          <input type="submit" value="<fmt:message key="user.profile.accept"></fmt:message>" class="btn btn-primary form-button">
        </div>
      </fieldset>
    </form>
  </div>
</body>
  <ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
