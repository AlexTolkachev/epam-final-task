<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 13.06.2016
  Time: 14:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <script type="text/javascript" src="${root}javascript/add_order_to_table.js"></script>
  <script type="text/javascript" src="${root}javascript/add_to_order.js"></script>
  <script type="text/javascript" src="${root}javascript/update_counter_list_tablets.js"></script>
  <script type="text/javascript" src="${root}javascript/ask_for_call.js"></script>

  <link rel="stylesheet" type="text/css" href="${root}css/nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/form_input.css">
  <link rel="stylesheet" type="text/css" href="${root}css/pop_up_style.css">

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>
  <c:set var="delivery"><fmt:message key="nav.delivery.link"/> </c:set>
  <c:set var="payment"><fmt:message key="nav.payment.link"/> </c:set>
  <c:set var="order_call"><fmt:message key="nav.order_call.link"/> </c:set>
  <c:set var="header_label"><fmt:message key="header.label"/> </c:set>
  <c:set var="header_description"><fmt:message key="header.description"/> </c:set>
  <c:set var="homelink"><fmt:message key="nav.home.link"/> </c:set>
  <c:set var="bucketLink"><fmt:message key="nav.bucket.link"/> </c:set>
  <c:set var="contactsLink"><fmt:message key="nav.contacts.link"/> </c:set>
  <c:set var="logoutLink"><fmt:message key="nav.logout.link"/> </c:set>

  <script>
    $(document).ready(function(){
      $(".count").on('input', function(){
        var pack_count = $(this).val();
        var order_id = $(this).attr('id');

        $.post("order?action=update_counter&pack_count=" + pack_count + "&order_id=" + order_id, function(data){
          alert("Change is successfull! Data: " + data );
        })
      });
    });

  </script>
</head>
<body>
<ex:navbar deliveryLink="${delivery}" paymentLink="${payment}" orderCallLink="${order_call}"
           username="${patient.username}" language="${language}"></ex:navbar>

<ex:myheader headerLabel="${header_label}" headerDescription="${header_description}" homeLink="${homelink}"
             bucketLink="${bucketLink}" contactsLink="${contactsLink}" logoutLink="${logoutLink}"></ex:myheader>

<c:set var="ask_call_message"><fmt:message key="patient.ask_call"/></c:set>
<ex:mymodal imageSource="${root}images/panel-info.png" message="${ask_call_message}"/>
<div style="margin-left: 40px;">
<div class="row">
  <div class="col-sm-3">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <a href="http://localhost:8086/patient/order?tabletId=${tablet.tabletId}" style="color: antiquewhite">${tablet.name}</a>
      </div>
      <div class="panel-body" style="background-color: aliceblue; width: inherit; max-width: inherit; height: 175px; max-height: 175px;">
        <div id="panel-body-${tablet.tabletId}"><img src="images\bottle_of_pills.png" align="left" class="img-responsive" style="width:30%" alt="Image"></div>
        <div style="display: inline;"><h5><fmt:message key="tablet.order.weight"/> : ${tablet.weight}<fmt:message key="patient.order.gramm"/></h5><h5><fmt:message key="tablet.order.pills_count"/>:${tablet.pillsCount}<fmt:message key="tablet.order.pills_count.pcs"/></h5></div>
        <div style="display: inline;"><h5><fmt:message key="tablet.edit.price"/> : ${tablet.price} $</h5><h5><fmt:message key="tablet.edit.tablet_type"/>: <fmt:message key="${tablet.tabletType.messageCode}"/> </h5></div>
      </div>
      <div class="panel-footer">
        <div id="order-footer-${tablet.tabletId}">
          <button type="button" id="myButton" class="btn btn-primary myButton" value="${tablet.tabletId}"><fmt:message key="tablet.order.button.make_order"/> </button>
        </div>
      </div>
    </div>
  </div>

  <c:set var="id" value="${user.id}"></c:set>
  <c:forEach items="${orders}" var="order">
    <c:choose>
      <c:when test="${order.tabletId == tablet.tabletId}">
        <c:choose>
          <c:when test="${order.patientId == id}">
            <script type="text/javascript">
              myFunc(${tablet.tabletId}, ${order.orderId}, ${order.packageCount});
            </script>
          </c:when>
        </c:choose>
      </c:when>
    </c:choose>
  </c:forEach>
    <div>
      <h2><fmt:message key="tablet.edit.tablet_type"/> : <fmt:message key="${tablet.tabletType.messageCode}"/> </h2>
      <h3><fmt:message key="tablet.edit.description"/> : <c:out value="${tablet.description}"/> </h3>
    </div>
  </div>
</div>
</body>

<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>

</html>
