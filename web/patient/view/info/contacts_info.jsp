<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 22.06.2016
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<html>
<head>
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <c:set var="img_root" value="http://localhost:${pageContext.request.localPort}/patient/images/"/>
  <c:set var="root" value="http://localhost:${pageContext.request.localPort}/patient/css/"/>
  <c:set var="js_root" value="http://localhost:${pageContext.request.localPort}/patient/javascript/"/>

  <link rel="stylesheet" type="text/css" href="${root}nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}header_nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}confirm_div.css">
  <link rel="stylesheet" type="text/css" href="${root}pop_up_style.css">

  <script type="text/javascript" src="${js_root}ask_for_call.js"></script>

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>
  <c:set var="delivery"><fmt:message key="nav.delivery.link"/> </c:set>
  <c:set var="payment"><fmt:message key="nav.payment.link"/> </c:set>
  <c:set var="order_call"><fmt:message key="nav.order_call.link"/> </c:set>
  <c:set var="header_label"><fmt:message key="header.label"/> </c:set>
  <c:set var="header_description"><fmt:message key="header.description"/> </c:set>
  <c:set var="homelink"><fmt:message key="nav.home.link"/> </c:set>
  <c:set var="bucketLink"><fmt:message key="nav.bucket.link"/> </c:set>
  <c:set var="contactsLink"><fmt:message key="nav.contacts.link"/> </c:set>
  <c:set var="logoutLink"><fmt:message key="nav.logout.link"/> </c:set>
</head>
<body>

<ex:navbar deliveryLink="${delivery}" paymentLink="${payment}" orderCallLink="${order_call}"
           username="${user.username}" language="${language}"></ex:navbar>

<ex:myheader headerLabel="${header_label}" headerDescription="${header_description}" homeLink="${homelink}"
             bucketLink="${bucketLink}" contactsLink="${contactsLink}" logoutLink="${logoutLink}"></ex:myheader>

<c:set var="info_symbol" value="${img_root}panel-info.png"/>
<c:set var="ask_call_message"><fmt:message key="patient.ask_call"/></c:set>
<ex:mymodal imageSource="${info_symbol}" message="${ask_call_message}"/>

<div style="margin: 50px;">
  <h2 style="margin: 50px;"><fmt:message key="contacts.menu.interact_with_us"/> </h2>

  <p><img src="${img_root}Telephone-icon.png"/> 8 495 926-36-42</p></br>
  <p><img src="${img_root}Very Basic home.png"><fmt:message key="contacts.menu.e-mail"/> </p>
  <p><img src="${img_root}message-32.png"/> <fmt:message key="contacts.menu.adress"/> </p>
  <p><img src="${img_root}very-basic-clock-icon.png"/><fmt:message key="contacts.menu.work_time"/> </p>
</div>

</body>

<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>

</html>
