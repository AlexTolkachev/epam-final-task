<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 17.06.2016
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/nav_style.css">

  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">
  <script type="text/javascript" src="${root}javascript/ask_for_call.js"></script>

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>

  <c:set var="delivery"><fmt:message key="nav.delivery.link"/> </c:set>
  <c:set var="payment"><fmt:message key="nav.payment.link"/> </c:set>
  <c:set var="order_call"><fmt:message key="nav.order_call.link"/> </c:set>
  <c:set var="header_label"><fmt:message key="header.label"/> </c:set>
  <c:set var="header_description"><fmt:message key="header.description"/> </c:set>
  <c:set var="homelink"><fmt:message key="nav.home.link"/> </c:set>
  <c:set var="bucketLink"><fmt:message key="nav.bucket.link"/> </c:set>
  <c:set var="contactsLink"><fmt:message key="nav.contacts.link"/> </c:set>
  <c:set var="logoutLink"><fmt:message key="nav.logout.link"/> </c:set>
</head>

<body>
<ex:navbar deliveryLink="${delivery}" paymentLink="${payment}" orderCallLink="${order_call}"
           username="${patient.username}" language="${language}"></ex:navbar>

<ex:myheader headerLabel="${header_label}" headerDescription="${header_description}" homeLink="${homelink}"
             bucketLink="${bucketLink}" contactsLink="${contactsLink}" logoutLink="${logoutLink}"></ex:myheader>

<c:set var="ask_call_message"><fmt:message key="patient.ask_call"/></c:set>
<ex:mymodal imageSource="${root}images/panel-info.png" message="${ask_call_message}"/>

<table class="table">
  <tr>
    <th><fmt:message key="user.profile.name"/> </th>
    <th><fmt:message key="user.profile.surname"/> </th>
    <th><fmt:message key="order_call.patient.age"/> </th>
    <th><fmt:message key="patient.ask_recepie.doctor.experience"/> </th>
    <th><fmt:message key="patient.ask_recepie.doctor.qualification"/> </th>
    <th><fmt:message key="nav.telephone"/> </th>
    <th>E-mail</th>
    <th></th>
  </tr>
  <c:forEach items="${doctors}" var="doctor">
    <tr>
      <td>${doctor.name}</td>
      <td>${doctor.surname}</td>
      <td>${doctor.age}</td>
      <td>${doctor.experience}</td>
      <td>${doctor.qualification}</td>
      <td>${doctor.telephone}</td>
      <td>${doctor.eMail}</td>
      <td>
        <form action="askrecepie" method="post">
          <input type="hidden" value="${doctor.userId}" name="doctor_id">
          <input type="hidden" value="${order_id}" name="order_id">
          <input type="submit" value="Call" class="button btn-success">
        </form>
      </td>
    </tr>
  </c:forEach>
</table>

</body>
<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
