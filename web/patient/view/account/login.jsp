<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.06.2016
  Time: 12:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" type="text/css" href="${root}css/form_input.css">
  <script>
    $(function() {
      $( "#tabs" ).tabs();
    });
  </script>
</head>
<body>

<form action="/changeLanguage">
  <select id="language" name="language" onchange="submit()" style="margin-left: 50px">
    <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
    <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
  </select>
</form>

<div id="tabs">
  <div class="container">
    <ul class="nav nav-tabs">
      <li><a href="#tabs-1"><fmt:message key="login.button.submit"></fmt:message></a></li>
      <li><a href="#tabs-2">Register </a></li>
    </ul>
    <div id="tabs-1">
      <p>${message}</p>
      <div class="account-status">
        <form method="post" action="account">
          <fieldset>
            <label class="form-label" for="username"><fmt:message key="login.label.username" />: </label>
            <input type="text" class="form-control form-input" name="username" id="username" placeholder="Login">
          </fieldset>
          <fieldset>
            <label class="form-label" for="password"><fmt:message key="login.label.password"></fmt:message> </label>
            <input type="password" class="form-control form-input" name="password" id="password" placeholder="Password">
            <input type="submit" class="btn btn-primary form-button" value="Login">
          </fieldset>
        </form>
      </div>
    </div>
    <div id="tabs-2">
      <div class="account-status">
        <form method="post" action="?action=register">
          <div style="margin: 50px;"><a style="margin-right: 25px;" href="/pharmasist/account"><fmt:message key="register_tab.for_pharmasist"/> </a> <a href="/doctor/account"><fmt:message key="register_tab.for_doctor"/> </a> </div>
          <c:set var="username_label"><fmt:message key="login.label.username"/> </c:set>
          <c:set var="password_label"><fmt:message key="login.label.password"/> </c:set>
          <c:set var="name_label"><fmt:message key="user.profile.name"/> </c:set>
          <c:set var="surname_label"><fmt:message key="user.profile.surname"/> </c:set>
          <c:set var="adress_label"><fmt:message key="user.profile.adress"/> </c:set>
          <c:set var="telephone_label"><fmt:message key="nav.telephone"/> </c:set>
          <c:set var="age_label"><fmt:message key="order_call.patient.age"/> </c:set>

          <ex:userinputfields usernameLabel="${username_label}" passwordLabel="${password_label}"
                              firstnameLabel="${name_label}" secondnameLabel="${surname_label}" adressLabel="${adress_label}"
                              ageLabel="${age_label}" telephoneLabel="${telephone_label}"/>
          <fieldset>
            <label class="form-label" for="phys_group"><fmt:message key="user.profile.phys_group"></fmt:message>: </label>
            <input type="number" min="1" max="5" name="phys_group" id="phys_group" value="${patient.physicalGroup}" class="form-control form-input">
          </fieldset>
          <fieldset>
            <label class="form-label" for="virtual_cash"><fmt:message key="user.profile.cash"></fmt:message>: </label>
            <input type="number" name="virtual_cash" id="virtual_cash" min="0" max="10000" value="${patient.virtualCash}" class="form-control form-input">
          </fieldset>
          <fieldset>
            <label class="form-label" for="credit_card_cash"><fmt:message key="tablet.accept_order.card"></fmt:message> : </label>
            <input type="number" name="credit_card_cash" min="0" max="10000" id="credit_card_cash" value="${patient.creditCardCash}" class="form-control form-input">
          </fieldset>

          <fieldset>
            <input type="submit" class="btn btn-primary form-button" value="Register">
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>

</body>
<c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
<c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
<c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>
<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
