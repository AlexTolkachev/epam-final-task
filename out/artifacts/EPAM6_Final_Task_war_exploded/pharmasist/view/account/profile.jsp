<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 20.06.2016
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" href="${root}css/form_input.css">
</head>
<body>

<form action="/changeLanguage">
  <select id="language" name="language" onchange="submit()" style="margin-left: 50px">
    <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
    <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
  </select>
</form>

<legend class="form-label"><fmt:message key="user.profile.info"></fmt:message>: ${pharmasist.username}</legend>
<div class="account-status">
  <form method="post" action="profile">

    <fieldset>
      <label class="form-label" for="name"><fmt:message key="user.profile.name"></fmt:message>: </label>
      <input type="text" name="name" value="${pharmasist.name}" id="name" class="form-control form-input" required>
    </fieldset>

    <fieldset>
      <label class="form-label" for="surname"><fmt:message key="user.profile.surname"></fmt:message>: </label>
      <input type="text" name="surname" value="${pharmasist.surname}" id="surname" class="form-control form-input" required>
    </fieldset>

    <fieldset>
      <label class="form-label" for="adress"><fmt:message key="user.profile.adress"></fmt:message>: </label>
      <input type="text" name="adress" value="${pharmasist.adress}" id="adress" class="form-control form-input" required>
    </fieldset>

    <fieldset>
      <label class="form-label" for="age"><fmt:message key="order_call.patient.age"></fmt:message> </label>
      <input type="number" name="age" value="${pharmasist.age}" id="age" min="1" max="100" class="form-control form-input" required>
    </fieldset>

    <fieldset>
      <label class="form-label" for="telephone"><fmt:message key="nav.telephone"></fmt:message> </label>
      <input type="tel" name="telephone" value="${pharmasist.telephone}" id="telephone" class="form-control form-input" required>
    </fieldset>

    <fieldset>
      <label class="form-label" for="e-mail">E-mail: </label>
      <input type="email" id="e-mail" name="e-mail" value="${pharmasist.eMail}" class="form-control form-input" required>
    </fieldset>

    <fieldset>
      <label for="pharmacy_occupation" class="form-label">Pharmacy Occupation:</label>
      <input type="text" class="form-control form-input" name="pharmacy_occupation" id="pharmacy_occupation" value="${pharmasist.pharmacyOccupation}" required>
    </fieldset>

    <input type="hidden" value="${pharmasist.id}" name="pharmasist_id">
    <input type="hidden" value="${pharamasist.roleId}" name="role_id">

    <fieldset>
      <input type="submit" class="btn btn-primary form-button" value="Register">
    </fieldset>
  </form>
</div>
</body>
</html>
