<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 15.06.2016
  Time: 17:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">
</head>
<body>
<div class="jumbotron">
  <form action="/changeLanguage">
    <select id="language" name="language" style="background-color: #003399; color: #f2f2f2" onchange="submit()">
      <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
      <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
      <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
    </select>
  </form>
  <div class="container text-center">
    <h1 style="color: #003399"><fmt:message key="header.label"></fmt:message></h1>
    <p style="color: #b30000"><fmt:message key="order_call.header.info"></fmt:message></p>
  </div>
</div>
<nav>
  <div class="my-header">
    <ul class="header-normal header">
      <li><a href="/doctor/order_calls"><fmt:message key="nav.home.link"></fmt:message> </a></li>
      <li><a href="account?action=logout"><fmt:message key="nav.logout.link"></fmt:message> </a></li>
      <li><a href="/doctor/profile"> ${user.username} </a> </li>
    </ul>
  </div>
</nav>
<c:choose>
  <c:when test="${fn:length(calls) == 0}">
    <div style="margin: 50px;">
      <h2><fmt:message key="doctor.order_calls.message"/></h2>
    </div>
  </c:when>
  <c:otherwise>
    <div class="container">
      <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
          <th><fmt:message key="order_call.table.tablet_name"></fmt:message></th>
          <th><fmt:message key="order_call.tablet.pack_count"></fmt:message></th>
          <th><fmt:message key="order_call.tablet.weight"></fmt:message></th>
          <th><fmt:message key="order_call.patient.name_surname"></fmt:message></th>
          <th><fmt:message key="order_call.patient.phys_group"></fmt:message></th>
          <th><fmt:message key="order_call.patient.age"></fmt:message></th>
          <th></th>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${calls}" var="call">
          <tr>
            <td>${call.order.tablet.name}</td>
            <td>${call.order.packageCount}</td>
            <td>${call.order.tablet.weight}</td>
            <td>${call.order.patient.name} ${call.order.patient.surname}</td>
            <td>${call.order.patient.physicalGroup}</td>
            <td>${call.order.patient.age}</td>
            <td><a href="/doctor/accept_recepie?idOrder=${call.orderId}">Accept</a></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
    </div>
  </c:otherwise>
</c:choose>

</body>
<footer class="container-fluid text-center">
  <p><fmt:message key="online_store.copyright.footer"></fmt:message></p>
  <form class="form-inline"><fmt:message key="get_deals.footer"></fmt:message>
    <input type="email" class="form-control" size="50" placeholder="Email Address">
    <button type="button" class="btn btn-danger"><fmt:message key="sign_up.footer"></fmt:message></button>
  </form>
</footer>
</html>
