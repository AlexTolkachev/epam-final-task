
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 31.05.2016
  Time: 11:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />

<html lang="${language}">
<head>
    <title></title>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" type="text/css" href="${root}css/confirm_div.css">

</head>
<body>
<div class="mydiv">
  <form action="/changeLanguage">
    <select id="language" name="language" style="background-color: #003399; color: #f2f2f2" onchange="submit()">
      <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
      <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
      <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
    </select>
  </form>
  <h2><fmt:message key="doctor.order_call.send_recepie"/> </h2>
  <h4>
    <p>
      <fmt:message key="order_call.table.tablet_name"/>: ${call.order.tablet.name}
    </p>
    <p>
      <fmt:message key="order_call.tablet.pack_count"/>: ${call.order.packageCount}'pc
      <fmt:message key="tablet.order.weight"/>: ${call.order.tablet.weight}'<fmt:message key="patient.order.gramm"/>
      <fmt:message key="tablet.order.pills_count"/>" ${call.order.tablet.pillsCount}'pc
    </p>
    <p>
      <fmt:message key="order_call.patient.name_surname"/>: ${call.order.patient.name}
      ${call.order.patient.surname}
      <fmt:message key="order_call.patient.age"/> : ${call.order.patient.age}
    </p>
    <p>
      <fmt:message key="tablet.edit.tablet_type"/> : ${call.order.tablet.tabletType}
    </p>
    <h3> <fmt:message key="tablet.edit.description"/>: </h3>
    <p>
      ${call.order.tablet.description}
    </p>
    <h3><a href="/doctor/order_calls"><fmt:message key="doctor.order_call.back"/> </a> </h3>
  </h4>
</div>

</body>
  <ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
