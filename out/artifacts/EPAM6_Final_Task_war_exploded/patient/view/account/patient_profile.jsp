<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 22.05.2016
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" href="${root}css/form_input.css">

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>

</head>
<body>
<form>
  <select id="language" name="language" onchange="submit()" style="margin-left: 50px">
    <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
    <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
  </select>
</form>
<legend class="form-label"><fmt:message key="user.profile.info"></fmt:message>: ${patient.username}</legend>
${message}<br>
  <div class="account-status">
    <form method="post" action="profile">
      <input type="hidden" name="username" value="${patient.username}">
      <fieldset>
        <label class="form-label" for="name"><fmt:message key="user.profile.name"></fmt:message>: </label>
        <input type="text" name="name" value="${patient.name}" id="name" class="form-control form-input" required>
      </fieldset>

      <fieldset>
        <label class="form-label" for="surname"><fmt:message key="user.profile.surname"></fmt:message>: </label>
        <input type="text" name="surname" value="${patient.surname}" id="surname" class="form-control form-input" required>
      </fieldset>

      <fieldset>
        <label class="form-label" for="age"><fmt:message key="order_call.patient.age"></fmt:message>: </label>
        <input type="number" name="age" id="age" min="1" max="100" value="${patient.age}" class="form-control form-input" required>
      </fieldset>

      <fieldset>
        <label class="form-label" for="adress"><fmt:message key="user.profile.adress"></fmt:message>: </label>
        <input type="text" name="adress" value="${patient.adress}" id="adress" class="form-control form-input" required>
      </fieldset>

      <fieldset>
        <label class="form-label" for="telephone"><fmt:message key="nav.telephone"></fmt:message> </label>
        <input type="tel" name="telephone" value="${patient.telephone}" id="telephone" class="form-control form-input" required>
      </fieldset>

      <fieldset>
        <label class="form-label" for="e-mail">E-mail: </label>
        <input type="email" id="e-mail" name="e-mail" value="${patient.eMail}" class="form-control form-input" required>
      </fieldset>
      <fieldset>
        <label class="form-label" for="phys_group"><fmt:message key="user.profile.phys_group"></fmt:message>: </label>
        <input type="number" min="1" max="3" name="phys_group" id="phys_group" value="${patient.physicalGroup}" class="form-control form-input" required>
      </fieldset>
      <fieldset>
        <label class="form-label" for="name"><fmt:message key="user.profile.cash"></fmt:message>: </label>
        <input type="number" name="virtual_cash" step="any" id="virtual_cash" min="0" max="10000" value="${patient.virtualCash}" class="form-control form-input" required>
      </fieldset>
      <fieldset>
        <label class="form-label" for="credit_card_cash"><fmt:message key="tablet.accept_order.card"></fmt:message> : </label>
        <input type="number" name="credit_card_cash" min="0" max="10000" step="any" id="credit_card_cash" value="${patient.creditCardCash}" class="form-control form-input" required>
      </fieldset>

      <input type="hidden" value="${patient.id}" name="patient_id">
      <input type="hidden" value="${patient.roleId}" name="role_id">

      <fieldset>
        <input type="submit" class="btn btn-primary form-button" value="<fmt:message key="user.profile.accept"></fmt:message>">
      </fieldset>
    </form>
  </div>
</body>
<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
