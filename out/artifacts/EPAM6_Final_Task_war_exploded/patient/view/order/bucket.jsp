<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 31.05.2016
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <c:set var="root" value="${pageContext.request.contextPath}"/>

  <script type="text/javascript" src="${root}javascript/update_counter_list_tablets.js"></script>
  <script type="text/javascript" src="${root}javascript/bucket_buttons_access.js"></script>
  <script type="text/javascript" src="${root}javascript/ask_for_call.js"></script>

  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/bucket_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/pop_up_style.css">

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>
  <c:set var="delivery"><fmt:message key="nav.delivery.link"/> </c:set>
  <c:set var="payment"><fmt:message key="nav.payment.link"/> </c:set>
  <c:set var="order_call"><fmt:message key="nav.order_call.link"/> </c:set>
  <c:set var="header_label"><fmt:message key="header.label"/> </c:set>
  <c:set var="header_description"><fmt:message key="header.description"/> </c:set>
  <c:set var="homelink"><fmt:message key="nav.home.link"/> </c:set>
  <c:set var="bucketLink"><fmt:message key="nav.bucket.link"/> </c:set>
  <c:set var="contactsLink"><fmt:message key="nav.contacts.link"/> </c:set>
  <c:set var="logoutLink"><fmt:message key="nav.logout.link"/> </c:set>
</head>
<body onload="checkForAccess(${access})">

<ex:navbar deliveryLink="${delivery}" paymentLink="${payment}" orderCallLink="${order_call}"
           username="${patient.username}" language="${language}"></ex:navbar>

<ex:myheader headerLabel="${header_label}" headerDescription="${header_description}" homeLink="${homelink}"
             bucketLink="${bucketLink}" contactsLink="${contactsLink}" logoutLink="${logoutLink}"></ex:myheader>

<c:set var="ask_call_message"><fmt:message key="patient.ask_call"/></c:set>
<ex:mymodal imageSource="${root}images/panel-info.png" message="${ask_call_message}"/>

<c:set var="id" value="${patient.id}"></c:set>
<c:set var="sum" value="0" scope="page"/>

<div class="container">
      <c:forEach items="${orders}" var="order">
        <div class="order-div">
          <div class="header-normal header">
            <h3>${order.tablet.name}</h3>
            <a href="bucket?action=delete&orderId=${order.orderId}" id="delete_rec">X</a>
          </div>
          <p class="order-p"><fmt:message key="tablet.order.need_recepie">:</fmt:message>
            <c:if test="${order.tablet.needRecepie == true}">
              <img src="images\ok-xl.png">
            </c:if> <c:if test="${order.tablet.needRecepie == false}">
              <img src="images\red_cross-64.png">
            </c:if>
            <fmt:message key="tablet.edit.tablet_type"/>: ${order.tablet.tabletType}
            <fmt:message key="tablet.order.pills_count"/>: ${order.tablet.pillsCount}pcs
            <fmt:message key="tablet.order.weight"></fmt:message>: ${order.tablet.weight}<fmt:message key="patient.order.gramm"/>
            <fmt:message key="tablet.order.allowed"></fmt:message>:
            <c:if test="${order.isAllowed == true}">
              <img src="images\ok-xl.png">
            </c:if>
            <c:if test="${order.isAllowed == false}">
              <img src="images\red_cross-64.png">
            </c:if></p>

          <c:set var="sum" value="${sum + order.tablet.price*order.packageCount}"></c:set>

          <input type="number" min='1' name="pack_count" value="${order.packageCount}" class="count" id="${order.orderId}">

          <%--When need recepie, add order_call button--%>
          <c:choose>
            <c:when test="${order.isAllowed == false}">
              <c:set var="access" value="false"></c:set>
              <form action="/patient/askrecepie" method="get">
                <input type="hidden" value="${order.tabletId}" name="tablet_id">
                <input type="hidden" value="${order.patientId}" name="patient_id">
                <input type="hidden" value="${order.orderId}" name="order_id">
                <input type="submit" style="margin-top: 25px" value="<fmt:message key="tablet.order.ask_recepie.button"></fmt:message>" class="btn btn-primary" id="submit_call_btn_${order.orderId}">
              </form>
              <c:forEach items="${order_calls}" var="order_call">
                <c:choose>
                  <c:when test="${order_call.orderId == order.orderId}">
                    <script>
                      disableCallButton(${order.orderId});
                    </script>
                  </c:when>
                </c:choose>
              </c:forEach>
            </c:when>
          </c:choose>
        </div>
      </c:forEach>
  <c:choose>
    <c:when test="${fn:length(orders)== 0}">
      <div style="margin: 50px">
        <h2><fmt:message key="patient.bucket.empty_bucket.message"/> </h2>
      </div>
    </c:when>
    <c:otherwise>
  <div class="account-status">
    <p><fmt:message key="tablet.order.info"></fmt:message></p>

    <p class="price" id="sum"><fmt:message key="tablet.order.price"></fmt:message>: ${sum}</p>

    <form METHOD="post" ACTION="/patient/order?action=registeredTab">
      <input type="hidden" value="${sum}" name="order_price" id="price">
      <input type="submit" value="<fmt:message key="tablet.order.button.make_order"></fmt:message>" id="orderBtn" class="btn btn-success">
      <p id="info"></p>
    </form>
    </c:otherwise>
  </c:choose>
  </div>
</div>
</body>
  <ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>
</html>
