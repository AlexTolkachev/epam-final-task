<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.05.2016
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/pop_up_style.css">

  <script type="text/javascript" src="${root}javascript/ask_for_call.js"></script>

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>
  <c:set var="delivery"><fmt:message key="nav.delivery.link"/> </c:set>
  <c:set var="payment"><fmt:message key="nav.payment.link"/> </c:set>
  <c:set var="order_call"><fmt:message key="nav.order_call.link"/> </c:set>
  <c:set var="header_label"><fmt:message key="header.label"/> </c:set>
  <c:set var="header_description"><fmt:message key="header.description"/> </c:set>

  <script>
    function compare(par1, par2){
      var price = par1;
      var cash = par2;
      if(parseFloat(cash, 10) >= parseInt(price, 10)){
        $('#confirm_btn').attr('disabled', false);
      }else{
        $('#confirm_btn').attr('disabled', true);
      }
    }
    function onRadioClick(isCash){

      var price = $('#hidden_price').val();
      var cash;
      if(isCash){
        cash = ${patient.virtualCash};
        document.getElementById('cash').innerHTML = "<fmt:message key="tablet.accept_order.cash"></fmt:message>: " + cash;
        document.getElementById('hidden_cash').value = cash;
        document.getElementById('hidden_payment_type').value = "cash";
      }else{
        cash = ${patient.creditCardCash};
        document.getElementById('cash').innerHTML = "<fmt:message key="tablet.accept_order.card"></fmt:message>: " + cash;
        document.getElementById('hidden_cash').value = cash;
        document.getElementById('hidden_payment_type').value = "card";
      }
      compare(price, cash);
    }
  </script>
  <style>
    .oformit-order{
      margin-top: 50px;
      margin-bottom: 50px;
      padding: 50px;
      background-color: #e1e1d0;
      margin-left: 50px;
      border-radius: 15px;
    }

    .header-text{
      padding: 30px;
      background-color: blue;
      color: white;
      font-size: 200%;
    }


    .list-order{
      padding-bottom: 15px;
      border-bottom: 2px solid gray;
      padding-top: 20px;
    }

    .form{
      padding-bottom: 20px;
    }

    .header-order{
      padding: 50px;
      margin-left: 100px;
      margin-right: 100px;
    }

    .credit{
      background-color: #e1e1d0;
      padding: 15px;
      margin-top: 30px;
    }

    .sum{
      background-color: #c3c3a2;
      padding: 5px;
    }
  </style>
</head>
<body>

<ex:navbar deliveryLink="${delivery}" paymentLink="${payment}" orderCallLink="${order_call}"
           username="${patient.username}" language="${language}"></ex:navbar>

<c:set var="ask_call_message"><fmt:message key="patient.ask_call"/></c:set>
<ex:mymodal imageSource="${root}images/panel-info.png" message="${ask_call_message}"/>

<c:set var="sum" value="${order_price}"></c:set>

<div class="row">
  <div class="col-sm-4 header-order">
    <form action="${pageContext.request.contextPath}/patient/order?action=orderConfirm" method="post" class="form-inline" role="form">
      <div class="form">
        <h2><fmt:message key="tablet.accept_order.payment_method"></fmt:message>:</h2>
      </div>
      <div class="form">
        <input type="radio" name="payment" value="virtualCash" onclick="onRadioClick(true)" class="form-control" checked> <fmt:message key="tablet.accept_order.cash"></fmt:message>
      </div>
      <div class="form">
        <input type="radio" name="payment" value="card" onclick="onRadioClick(false)" class="form-control"> <fmt:message key="tablet.accept_order.card"></fmt:message>
      </div>
      <input type="submit" name="orderConfirmBtn" value="Confirm" id="confirm_btn" class="btn btn-success">
      <input type="hidden" name="hidden_payment_type" value="cash" id="hidden_payment_type">
      <input type="hidden" value="${sum}" id="hidden_price" name="hidden_price">
    </form>
    <div class="credit">
      <p class="order-p"><fmt:message key="tablet.accept_order.info"></fmt:message></p>
      <div class="sum">
        <p class="order-p" id="price"><fmt:message key="tablet.order.price"></fmt:message>: ${sum}</p>
      </div>

      <h3 id="cash"><fmt:message key="tablet.accept_order.cash"></fmt:message>: ${patient.virtualCash}</h3>
      <input type="hidden" value="${patient.virtualCash}" id="hidden_cash">
    </div>

  </div>
  <script>
    compare(${order_price}, ${patient.virtualCash});
  </script>
  <div class="col-sm-4 oformit-order">
    <h4 class="header-text"><fmt:message key="patient.order.info"/>: </h4>
    <c:forEach items="${orders}" var="order">
    <div class="list-order">
      <c:set var="price" value="${order.tablet.price * order.packageCount}"></c:set>
      <p class="order-p">${order.tablet.name};  ${order.tablet.weight} <fmt:message key="patient.order.gramm"/> </p>
      <p class="order-p">${order.packageCount}' <fmt:message key="patient.order.packs"/>  ${price}<fmt:message key="patient.order.value"/> </br></p>
      <input type="hidden" value="${order.orderId}" name="order_id">
      <input type="number" name="pack_count" value="${order.packageCount}" readonly>
    </div>
    </c:forEach>
  </div>
</div>

<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>

</body>
</html>
