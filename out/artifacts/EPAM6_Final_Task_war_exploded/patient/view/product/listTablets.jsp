<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 14.05.2016
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ex" uri="customtags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="by.epam.i18n.text" />
<html lang="${language}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <c:set var="root" value="${pageContext.request.contextPath}"/>
  <script type="text/javascript" src="${root}javascript/add_order_to_table.js"></script>
  <script type="text/javascript" src="${root}javascript/add_to_order.js"></script>
  <script type="text/javascript" src="${root}javascript/update_counter_list_tablets.js"></script>
  <script type="text/javascript" src="${root}javascript/ask_for_call.js"></script>

  <link rel="stylesheet" type="text/css" href="${root}css/nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/page_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/header_nav_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/form_input.css">
  <link rel="stylesheet" type="text/css" href="${root}css/pop_up_style.css">
  <link rel="stylesheet" type="text/css" href="${root}css/tablet_types.css">

  <c:set var="copyright"><fmt:message key="online_store.copyright.footer"/></c:set>
  <c:set var="deals"><fmt:message key="get_deals.footer"/> </c:set>
  <c:set var="sign_up"><fmt:message key="sign_up.footer"/> </c:set>
  <c:set var="delivery"><fmt:message key="nav.delivery.link"/> </c:set>
  <c:set var="payment"><fmt:message key="nav.payment.link"/> </c:set>
  <c:set var="order_call"><fmt:message key="nav.order_call.link"/> </c:set>
  <c:set var="header_label"><fmt:message key="header.label"/> </c:set>
  <c:set var="header_description"><fmt:message key="header.description"/> </c:set>
  <c:set var="homelink"><fmt:message key="nav.home.link"/> </c:set>
  <c:set var="bucketLink"><fmt:message key="nav.bucket.link"/> </c:set>
  <c:set var="contactsLink"><fmt:message key="nav.contacts.link"/> </c:set>
  <c:set var="logoutLink"><fmt:message key="nav.logout.link"/> </c:set>
  <script type="text/javascript">
    onload = function(){
      var e = document.getElementById("refreshed");
      if(e.value == "no") e.value = "yes";
      else{e.value = "no"; location.reload();}
    }
  </script>
</head>
<body>
<input type="hidden" id="refreshed" value="no">

<ex:navbar deliveryLink="${delivery}" paymentLink="${payment}" orderCallLink="${order_call}"
           username="${patient.username}" language="${language}"></ex:navbar>

<ex:myheader headerLabel="${header_label}" headerDescription="${header_description}" homeLink="${homelink}"
             bucketLink="${bucketLink}" contactsLink="${contactsLink}" logoutLink="${logoutLink}"></ex:myheader>

<c:set var="ask_call_message"><fmt:message key="patient.ask_call"/></c:set>
<ex:mymodal imageSource="${root}images/panel-info.png" message="${ask_call_message}"/>

<c:set var="id" value="${patient.id}"/>




<div class="container"style="margin-left: 30px;">
  <div class="row">
    <div class="col-sm-3" >

      <div class="search-menu">
        <p><fmt:message key="tablet.search.tablet_search"/> :</p>
        <c:set var="tabl_name"><fmt:message key="order_call.table.tablet_name"/> </c:set>
        <form method="get" action="/patient/tabletList">
          <fieldset>
            <input type="text" name="tablet_name" class="form-control form-input" style="margin-left: 0px;width: 90%;" placeholder="${tabl_name}">
            <input type="submit" class="btn btn-primary form-button" style="margin-left: 0px;" value=<fmt:message key="tablet.search.find"/> >
          </fieldset>
        </form>
      </div>

      <div class="types-menu">
        <ul>
          <li><a href="/patient/tabletList"><fmt:message key="tablet_type.enum.all"/> </a></li>
          <c:forEach items="${types}" var="type">
            <li><a href="/patient/tabletList?type=${type.typeId}"><fmt:message key="${type.messageCode}"/> </a></li>
          </c:forEach>
        </ul>
      </div>
    </div>
    <div style="margin-left: 350px;">
      <c:choose>
        <c:when test="${fn:length(tablets) == 0}">
          <div class="col-sm-8">
            <h2 style="display: inline;"><fmt:message key="tablet.search.nothing_found"/> </h2>
          </div>
        </c:when>
      </c:choose>
      <c:forEach items="${tablets}" var="tablet">
        <div class="col-sm-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <a href="http://localhost:8086/patient/order?tabletId=${tablet.tabletId}" style="color: antiquewhite">${tablet.name}</a>
            </div>
            <div class="panel-body" style="background-color: aliceblue; width: inherit; max-width: inherit; height: 175px; max-height: 175px;">
              <div id="panel-body-${tablet.tabletId}"><img src="images\bottle_of_pills.png" align="left" class="img-responsive" style="width:30%" alt="Image"></div>
              <div style="display: inline;"><h5><fmt:message key="tablet.order.weight"/> : ${tablet.weight}<fmt:message key="patient.order.gramm"/></h5><h5><fmt:message key="tablet.order.pills_count"/>:${tablet.pillsCount}<fmt:message key="tablet.order.pills_count.pcs"/></h5></div>
              <div style="display: inline;"><h5><fmt:message key="tablet.edit.price"/> : ${tablet.price} $</h5><h5><fmt:message key="tablet.edit.tablet_type"/>: <fmt:message key="${tablet.tabletType.messageCode}"/> </h5></div>
            </div>
            <div class="panel-footer">
              <div id="order-footer-${tablet.tabletId}">
                <button type="button" id="myButton" class="btn btn-primary myButton" value="${tablet.tabletId}"><fmt:message key="tablet.order.button.make_order"/> </button>
              </div>
            </div>
          </div>
        </div>
        <c:forEach items="${orders}" var="order">
          <c:choose>
            <c:when test="${order.tabletId == tablet.tabletId}">
              <c:choose>
                <c:when test="${order.patientId == id}">
                  <script type="text/javascript">
                    myFunc(${tablet.tabletId}, ${order.orderId}, ${order.packageCount});
                  </script>
                </c:when>
              </c:choose>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:forEach>
    </div>

  </div>
</div>

<div style="margin-left: 350px; margin-top: 25px;">

  <c:if test="${currentPage != 1}">
         <h3 style="display: inline; margin-right: 50px"><a href="/patient/tabletList?page=${currentPage - 1}">Previous</a></h3>
  </c:if>

  <c:forEach begin="1" end="${noOfPages}" var="i">
    <c:choose>
      <c:when test="${currentPage == i}">
          <h3 style="display: inline;"><c:out value="${i}"></c:out></h3>
      </c:when>
      <c:otherwise>
        <h3 style="display: inline;"><a href="/patient/tabletList?page=${i}"><c:out value="${i}"></c:out></a></h3>
      </c:otherwise>
    </c:choose>   
  </c:forEach>   

    <c:if test="${currentPage lt noOfPages}">
        <h3  style="display: inline;"><a href="/patient/tabletList?page=${currentPage + 1}">Next</a></h3>
    </c:if>
</div>

</body>

<ex:myfooter copyrightText="${copyright}" getDealsText="${deals}" signUpText="${sign_up}"></ex:myfooter>

</html>
