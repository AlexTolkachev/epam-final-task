$(document).ready(function(){
    $(".count").on('blur', function(){
        var pack_count = $(this).val();
        var order_id = $(this).attr('id');
        var html_input_element = this;
        $.post("order?action=update_counter&pack_count=" + pack_count + "&order_id=" + order_id, function(data){
            alert("Old count: " + data['pack_count'])
            var old_input = data['pack_count'];
            var price = data['order_sum'];
            $("#sum").html("Price: " + price);
            $("#price").val(price);
            $(html_input_element).val(old_input);
        })
    });
});